package com.spechtuhdelangbein.gamepad_tablet.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.spechtuhdelangbein.gamepad_tablet.BuildConfig;
import com.spechtuhdelangbein.gamepad_tablet.Configurations.Configuration_Global_Objects;
import com.spechtuhdelangbein.gamepad_tablet.R;

import java.util.Locale;

/*
*
* Created by Andreas Langbein
* 24.07.2015
*
* */

public class Activity_Options extends Activity {

    TextView tv_appversion;
    TextView btn_language;
    TextView btn_confirm;
    TextView btn_license;
    TextView btn_change_keycodes;

    String[] available_languages = new String[]{"English","Deutsch"};
    String[] corresponding_regioncode = new String[]{"en","de"};
    int language_index;

    SharedPreferences prefs;
    String languageKey = Configuration_Global_Objects.languageKey;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);

        btn_language = (TextView) findViewById(R.id.btn_language);
        btn_confirm  = (TextView) findViewById(R.id.btn_Confirm);
        btn_license  = (TextView) findViewById(R.id.btn_license);
        tv_appversion = (TextView) findViewById(R.id.tv_AppVersion);
        btn_change_keycodes = (TextView) findViewById(R.id.tv_KeyCodes);


        int versionCode = BuildConfig.VERSION_CODE;
        //Current AppVersion set as Text to the TextView
        tv_appversion.setText(tv_appversion.getText() + " " + versionCode);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        //Return the RegionCode from the saved Settings
        String language = prefs.getString(languageKey, "NULL");


        //If no language was saved before we take the default device language
        if (language.equals("NULL")) {
            String def_language = Locale.getDefault().getLanguage();
            language = regionCodeToFullName(def_language);
            btn_language.setText(btn_language.getText() + " " + language);

        //If a language was saved we load it from the Settings
        } else {
            //Shows ENGLISH instead of EN and so on
            language = regionCodeToFullName(language);
            btn_language.setText(btn_language.getText() + " " + language);
        }

        //This button opens an alertdialog showing all language choices
        btn_language.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder lang_choice=new AlertDialog.Builder(Activity_Options.this);
                lang_choice.setTitle(getResources().getString(R.string.opt_languagechoice_head)).setItems(available_languages, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        /*
                        * When a choice is made we take its index and set the buttonText to the corresponding language.
                        * However languages are best saved as region codes so we use the index to save the region code additionally
                        * */
                        language_index = which;
                        String[] tmp = ((String) btn_language.getText()).split("\\:");
                        btn_language.setText(tmp[0] + ": " + available_languages[language_index]);
                        prefs.edit().putString(languageKey, corresponding_regioncode[language_index]).commit();
                        Log.i("TAG", "language set "+ corresponding_regioncode[language_index]);
                    }

                });

                lang_choice.show();

            }

        });



        //Confirms all made changes
        //Also restarts the MainMenuActivity to confirm all language changes
        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Milestone","Loading "+corresponding_regioncode[language_index]);
                Locale myLocale = new Locale(corresponding_regioncode[language_index]);
                Resources res = getResources();
                DisplayMetrics dm = res.getDisplayMetrics();
                Configuration conf = res.getConfiguration();
                conf.locale = myLocale;
                res.updateConfiguration(conf, dm);
                Intent mainMenu = new Intent(v.getContext(), Activity_Main.class);
                startActivity(mainMenu);
                finish();
            }
        });


        /*
        * Opens an alertdialog to show the current license information from Strings.xml
        * Can be closed with a button
        * */
        btn_license.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder license_builder = new AlertDialog.Builder(v.getContext());
                license_builder.setMessage(getResources().getString(R.string.opt_license))
                        .setTitle(getResources().getString(R.string.opt_license_head));

                AlertDialog alert = license_builder.create();



                license_builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();

            }
        });

        btn_change_keycodes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent changeKeycodes = new Intent(view.getContext(), Activity_KeyCode.class);
                startActivity(changeKeycodes);
            }
        });


    }



    /**
            * On startup we load the button text representing the language out of the prefs.
            * But the loaded version is the regionCode NOT the full language name
            * That would show language:en instead of language:ENGLISH
            * So the following lines find the corresponding language name to the region code
            * */
    public String regionCodeToFullName(String language)
    {

        String res = "";
        int i = 0;
        for(String s : corresponding_regioncode)
        {
            if(s.equals(language.toLowerCase()))
            {
                res = available_languages[i];
                break;
            }
            i++;
        }

        return res;
    }
}
