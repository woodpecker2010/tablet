package com.spechtuhdelangbein.gamepad_tablet.DebugTools;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

import com.spechtuhdelangbein.gamepad_tablet.R;

/**
 * This class is responsible for showing the mouse movements send from the smartphones
 * It simply draws a rectangle on the screen in which a white dot will move around.
 *
 * Created by Benedikt Specht on 06.08.15.
 */
public class TouchpadView extends View {
    private int height = 1200;
    private int width = 1200;
    private float currX = width /2;
    private float currY = height / 2;
    private int radius = 20;
    private Paint paint;
    private Bitmap background;

    public TouchpadView(Context context, AttributeSet attrs) {
        super(context, attrs);
        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.WHITE);
        background = BitmapFactory.decodeResource(getResources(), R.drawable.half_transparent);


        // Set size of the view
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        if (size.x <= size.y)
            height = width = size.x - 40;
        else
            height = width = size.y - 40;
        currX = currY = width / 2;

    }

    public void changePositions(float addToX, float addToY){
        currX -= addToX;
        currY -= addToY;
        invalidate();
    }

    public void onDraw(Canvas canvas){
        super.onDraw(canvas);
        //background.draw(canvas);
        Rect rect = new Rect();
        rect.set(0, 0, width, height);
        canvas.drawBitmap(background, null, rect, paint);
        //canvas.drawBitmap(background, 0, 0, paint);

        // Draw the point
        if (currX > width)
            currX = width;
        if (currX < 0)
            currX = 0;
        if (currY > height)
            currY = height;
        if (currY < 0)
            currY = 0;
        canvas.drawCircle(currX,currY,radius,paint);

        // Feld Umrandung
        canvas.drawLine(0, 0, width-4, 0, paint);
        canvas.drawLine(0, height-4, width-4, height-4, paint);
        canvas.drawLine(0, 0, 0, height-4, paint);
        canvas.drawLine(width-4, 0, width-4, height-4, paint);
    }
}
