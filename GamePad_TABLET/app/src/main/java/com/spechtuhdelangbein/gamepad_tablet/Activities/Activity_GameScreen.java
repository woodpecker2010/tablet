package com.spechtuhdelangbein.gamepad_tablet.Activities;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.spechtuhdelangbein.gamepad_tablet.Configurations.Configuration_Global_Objects;
import com.spechtuhdelangbein.gamepad_tablet.DebugTools.CharacterView;
import com.spechtuhdelangbein.gamepad_tablet.DebugTools.TouchpadView;
import com.spechtuhdelangbein.gamepad_tablet.Utilities.GameView;
import com.spechtuhdelangbein.gamepad_tablet.R;

/**
 * This Activity is just a dummy activity for debugging which displays the incoming data.
 *
 */

public class Activity_GameScreen extends ActionBarActivity {

    TouchpadView tpv;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {

            if(Configuration_Global_Objects.mThreadConnectedThread != null)
                Configuration_Global_Objects.mThreadConnectedThread.cancel();
            this.finish();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    CharacterView kbv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gamepad);
        GameView gameView = (GameView) findViewById(R.id.game_view);
        gameView.setActivityGameScreen(this);
        tpv = (TouchpadView) findViewById(R.id.touch_view);
        kbv = (CharacterView) findViewById(R.id.keyboard_view);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_gamepad, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void startTouchpadActivity (View view) {
        Intent intent = new Intent(this, Activity_Main.class);
        startActivity(intent);
    }

    public void refreshTouchpadView (float addX, float addY){
        kbv.setVisibility(View.INVISIBLE);
        tpv.setVisibility(View.VISIBLE);
        tpv.changePositions(addX, addY);
        tpv.invalidate();
    }

    public void refreshKeyboardView (String character){
        tpv.setVisibility(View.INVISIBLE);
        kbv.setVisibility(View.VISIBLE);
        kbv.changeChar(character);
        kbv.invalidate();
    }

    public void setTouchpadViewInvisilbe(){
        tpv.setVisibility(View.INVISIBLE);
    }

    public void setCharacterViewInvisible(){
        kbv.setVisibility(View.INVISIBLE);
    }
}
