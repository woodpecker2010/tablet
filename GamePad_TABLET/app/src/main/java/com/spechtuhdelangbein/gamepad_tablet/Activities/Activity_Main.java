package com.spechtuhdelangbein.gamepad_tablet.Activities;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.spechtuhdelangbein.gamepad_tablet.Configurations.Configuration_Global_Objects;
import com.spechtuhdelangbein.gamepad_tablet.R;
import com.spechtuhdelangbein.gamepad_tablet.Utilities.Thread_AcceptingThread;
import com.spechtuhdelangbein.gamepad_tablet.Utilities.Thread_ConnectedThread;
import com.spechtuhdelangbein.gamepad_tablet.Utilities.Helper_NotificationClass;

import java.util.Locale;


public class Activity_Main extends Activity {

    Button m_HostBtn;
    BluetoothAdapter mBluetoothAdapter;
    Helper_NotificationClass hnc = new Helper_NotificationClass();
    public Thread_ConnectedThread m_conth;

    int size = 20;
    TextView start;
    TextView setDiscoverable;
    TextView settings;


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(bReceiver);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setLanguage();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d("Milestone", "Started onCreate");
        //Keep the screen on on all times
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(mBluetoothAdapter == null) {
            Toast msg = Toast.makeText(this,"It seems like your device does not support bluetooth",Toast.LENGTH_SHORT);
            msg.show();
        }

        start           = (TextView) findViewById(R.id.txt_start);
        settings        = (TextView) findViewById(R.id.txt_settings);
        setDiscoverable = (TextView) findViewById(R.id.txt_setDiscoverable);

        setDiscoverable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Host(v);
            }
        });

        setTextSize();

        /*
        * Here we would use the ACTION_DISCOVERY_STARTED to react to the device enabling its discoverability
        * but those events don't appear to be send. Instead we use ACTION_SCAN_MODE_CHANGED and check that state
        * in the receiver. If it is equal to SCAN_MODE_DISCOVERABLE we're good to go and start the accepting thread
        * */
        IntentFilter MyIntent = new IntentFilter();
        MyIntent.addAction(mBluetoothAdapter.ACTION_SCAN_MODE_CHANGED);
        registerReceiver(bReceiver, MyIntent);

        if(mBluetoothAdapter.isEnabled()) {
            Thread_AcceptingThread at = new Thread_AcceptingThread(mBluetoothAdapter, Activity_Main.this);
            at.start();
        }

    }

    @Override
    public void onResume(){
        super.onResume();
        if(mBluetoothAdapter.isEnabled()) {
            Thread_AcceptingThread at = new Thread_AcceptingThread(mBluetoothAdapter, Activity_Main.this);
            at.start();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void Host(View v)
    {

        //The next 3 lines simply make your device visible to all surrounding devices with bluetooth
        Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
        startActivity(discoverableIntent);

        if(mBluetoothAdapter.getScanMode() == mBluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Log.d("Milestone","You are discoverable");
            Thread_AcceptingThread at = new Thread_AcceptingThread(mBluetoothAdapter, Activity_Main.this);
            at.start();
        }

    }

    public void Disconnected()
    {
        if(mBluetoothAdapter.isEnabled()) {
            Thread_AcceptingThread at = new Thread_AcceptingThread(mBluetoothAdapter, Activity_Main.this);
            at.start();
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hnc.Notify(Activity_Main.this, "Lost connection to Phone. Please re-establish", "Sorry", 1);
            }
        });
    }


    public void Connected()
    {
        runOnUiThread(new Runnable(){
            @Override
            public void run(){
                hnc.RemoveNotification(Activity_Main.this, 1);
            }
        });

        startGamepadActivity(null);
    }

    public void startGamepadActivity(View view){
        Intent intent = new Intent(this, Activity_GameScreen.class);
        startActivity(intent);
    }

    public void startConnectionActivity(View view){
        Intent intent = new Intent(this, Activity_Main.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void startOptionActivity(View view){
        Intent options = new Intent(this, Activity_Options.class);
        startActivity(options);
    }

    /**
     * Checks whether or not a specific language was pre-set.
     * If so, the displayed text will be translated into the given one.
     * Otherwise the default one will be used
     *
     * This method will be also called from the optionsMenu
     * */
    private void setLanguage() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String languageKey = Configuration_Global_Objects.languageKey;
        String language = prefs.getString(languageKey, null);

        if(language != null) {
            Log.i("TAG", "language is "+language);
            Locale myLocale = new Locale(language);
            Resources res = getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();
            conf.locale = myLocale;
            res.updateConfiguration(conf, dm);
        } else {
            Log.e("Problem", "Language not found");
        }
    }

    /**
     * set up the textSize of the TextViews
     * that they appear in the same size on every screen
     */
    private void setTextSize(){
        float density = getResources().getDisplayMetrics().density;
        setDiscoverable.setTextSize(size * density);
        start.setTextSize(size * density);
        settings.setTextSize(size * density);
    }


    final BroadcastReceiver bReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothAdapter.ACTION_SCAN_MODE_CHANGED.equals(action)) {
                //Since we are discoverable now we can start an Thread_AcceptingThread
                //This thread listens for incoming client requests
                if(mBluetoothAdapter.getScanMode() == mBluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
                    Log.d("Milestone","You are discoverable");
                    Thread_AcceptingThread at = new Thread_AcceptingThread(mBluetoothAdapter, Activity_Main.this);
                    at.start();
                }
            }
        }
    };
}
