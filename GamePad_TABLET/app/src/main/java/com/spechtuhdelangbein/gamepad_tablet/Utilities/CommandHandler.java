package com.spechtuhdelangbein.gamepad_tablet.Utilities;

import android.util.Log;

import com.spechtuhdelangbein.gamepad_tablet.Configurations.Configuration_Gamepad;
import com.spechtuhdelangbein.gamepad_tablet.Configurations.Configuration_Global_Objects;

/**
 * This class handles the commandy throug a byte Array.
 * This array also represents the used Protocol. Any Data arriving from the Phone will be
 * stored in this format.
 *
 * If you want to exchange information between
 * the Tablet and the Phone use this protocol.
 *
 * The following Tables are showing the structure of the protocol.
 *
 * GAMEPAD.
 *
 * Offset	Position	Size    Name
 * ------   ------      ----    -------------
 *      0	0x00.0	    4	    Message Type
 *      0	0x00.4	    4	    Player No
 *      1	0x01.0	    1	    D-Pad up
 *      1	0x01.1	    1	    D-Pad down
 *      1	0x01.2	    1	    D-Pad left
 *      1	0x01.3	    1	    D-Pad right
 *      1	0x01.4	    1	    Start
 *      1	0x01.5	    1	    Back
 *      1	0x01.6	    1	    Left Stick Press
 *      1	0x01.7	    1	    Right Stick Press
 *      2	0x02.0	    1	    LB
 *      2	0x02.1	    1	    RB
 *      2	0x02.2	    1	    X-Box Logo
 *      2	0x02.3	    1	    UNUSED
 *      2	0x02.4	    1	    A
 *      2	0x02.5	    1	    B
 *      2	0x02.6	    1	    X
 *      2	0x02.7	    1	    Y
 *      3	0x03.0	    8	    Left - Trigger
 *      4	0x04.0	    8	    Right - Trigger
 *      5	0x05.0	    16	    Left Stick x-Axis
 *      7	0x07.0	    16	    Left Stick y-Axis
 *      9	0x09.0	    16	    Right Stick x-Axis
 *      11	0x0b.0	    16	    Right Stick y-Axis
 *
 *
 * TOUCHPAD:
 *
 * Offset	Position	Size    Name
 * ------   ------      ----    -------------
 *      0	0x00.0	    4	    Message Type
 *      0	0x00.7	    4	    PlayerNo
 *      1	0x01.0	    32	    Movement X-Axis
 *      5   0x05.0      32      Movement Y-Axis
 *      9   0x09.0      1       left Click
 *      9   0x09.1      1       right CLick
 *
 *
 * Keyboard
 *
 * Offset	Position	Size    Name
 * ------   ------      ----    -------------
 *      0	0x00.0	    4	    Message Type
 *      0	0x00.7	    4	    PlayerNo
 *      1	0x01.0	    16	    single letter
 *
 * For detailed information look into the documentation
 * Created by Benedikt on 15.06.2015.
 */


public class CommandHandler {

    //Message Types
    public final static int TYPE_GAMEPAD   = 1;
    public final static int TYPE_KEYBOARD  = 2;
    public final static int TYPE_MOUSE     = 3;
    public final static int TYPE_INFORMATION        = 4;
    public final static int TYPE_CONNECTION_REQUEST = 5;
    public final static int TYPE_QUIT_CONNECTION    = 6;

    private final int AMOUNT_TYPES  = 6;
    private final short BYTE_ARRAY_LENGTH = Configuration_Global_Objects.BYTE_ARRAY_LENGTH;

    // Player Numbers for setting the Message Type
    public final static int PLAYER_1 = 1;
    public final static int PLAYER_2 = 2;
    public final static int PLAYER_3 = 3;
    public final static int PLAYER_4 = 4;

    /** This array stores the current commands.*/
    private byte commands[] = new byte[BYTE_ARRAY_LENGTH];
    /** This variable stores the current playerNumber */
    private int playerNumber;




    public void CommandHandler(){
        commands = new byte[BYTE_ARRAY_LENGTH];
    }

    /**
     * resets the whole byte field with a new instance
     */
    private byte[] clearAll(){
        return commands = new byte[BYTE_ARRAY_LENGTH];
    }

    /**
     * Resets the whole command array and sets a new message type
     * @param type use one of the types:
     *             {@link CommandHandler#TYPE_GAMEPAD },
     *             {@link CommandHandler#TYPE_KEYBOARD },
     *             {@link CommandHandler#TYPE_MOUSE }
     * @return Returns true if successful, else false
     */
    public boolean setNewMessageType(int type, int player){
        if(type < 0 || type > AMOUNT_TYPES)
            return false;

        clearAll();
        //Set message type
        commands[0] = (byte) (commands[0] | type << 0);
        //set player number
        commands[0] = (byte) (commands[0] | player << 4);

        return true;
    }


    /**
     * Copies the bit values between 0 and 255 into a Byte.
     * @param number the number which shall be converted
     * @return returns the number as a Byte.
     */
    public byte convertToByte(int number){
        return (byte) (number & 0xFF);
    }

    /**
     * Clears the Bit at the position pos
     * @param pos Position of the Bit which is to be cleared (shall be between 0 and 7).
     * @param offset the offset in the byte array. for entry in the first byte offset = 0, for the second offset = 1 etc
     * @return returns true if successful, else false
     */
    private boolean clearBit (int offset, int pos){
        // if input is out of bounds return false
        if (pos > 7 || offset >= BYTE_ARRAY_LENGTH)
            return false;

        // clear the Bit in the specified byte field
        commands[offset] = (byte) (commands[offset] & ~(1 << pos));
        return true;

    }

    /**
     * Sets a Bit at the position pos in the Byte b.
     * @param pos Position of the Bit which is to set (shall be between 0 and 7).
     * @param offset the offset in the byte array. for entry in the first byte offset = 0, for the second offset = 1 etc
     * @return returns true if successful, else false
     */
    private boolean setBit(int offset, int pos){
        // if input is out of bounds return false
        if (pos > 7 || offset >= BYTE_ARRAY_LENGTH)
            return false;

        // set the Bit in the specified byte field
        //int test = 1 << pos;
        //commands[offset] = 0;
        commands[offset] = (byte) (commands[offset] | 1 << pos);
        return true;
    }

    /**
     * Checks whether the Bit at position pos is set.
     * @param pos position of the Bit which is to be checked (shall be between 0 and 7).
     * @param offset the offset in the byte array. for entry in the first byte offset = 0, for the second offset = 1 etc
     * @return if Bit is set this method returns true, in any other case false.
     */
    private boolean checkBitSet(int offset, int pos) {
        // if input is out of bounds return false
        if (pos > 7 || offset >= BYTE_ARRAY_LENGTH)
            return false;

        return (commands[offset] & 1 << pos) != 0;

    }

    /*********************************************************************
     **             START OF THE PUBLIC METHODS                         **
     *********************************************************************/
    /**
     * This method is for sending keyboard input. It simply
     * converts the character into an integer
     * and saves the first 16Bit of this integer into the first 2 bytes of the command array.
     *
     * If the given char has this structure:
     * 0x0A23 then 0x0A will lie in command[1] and 0x23 in command[2]
     *
     * Make sure that your messageType is set to {@link CommandHandler#TYPE_KEYBOARD}
     * and get's resetted to another type after changing to another input method.
     *
     * Uses the {@link CommandHandler#saveFirst16Bit(int)} method
     *
     * @param c Character which is to be send
     * @return Returns true as soon as the method finishes
     */
    public boolean setChar(char c){
        int convertedCharacter = c & 0xFFFF;
        byte[] b = saveFirst16Bit(convertedCharacter);
        int offset = 1;
        commands[offset]    = b[1];
        commands[offset +1] = b[0];
        return true;
    }

    /**
     * Sets a Bit at the position pos in the Byte b.
     * @param button Insert an int array which contains the offset at [0] and the bitposition at [1]
     * @return returns true if successful, else false
     */
    public boolean setBit(int[] button){
        if(button.length < 2)
            return false;

        return setBit(button[0],button[1]);
    }

    /**
     * This method is meant for storing the touchpad axis movement input.
     * Stores a float value into the command array.
     *
     * It simply converts the float value into an integer and then splits it up into arrays. So its
     * not useful if an exact float value shall be stored.
     *
     * Make sure that the messageType is set to {@link CommandHandler#TYPE_MOUSE}
     * and that it's resetted after changing to another input method.
     *
     * @param number float value of the movement in the specific direction (X-Axis or Y-Axis
     * @param offset Set the Axis of the Value: {@link Configuration_Gamepad#XAXIS} or
     *               {@link Configuration_Gamepad#YAXIS}
     * @return Returns true if successful and false if float couldn't be casted to an Integer
     */
    public boolean saveFloat(float number, int offset){
        // Convert the float value into an integer
        int converted;
        try {
            converted = (int) number;
        } catch (Exception e) {
            Log.e("Error","Float number too big for Integer");
            return false;
        }

        // Insert Integer into command Array
        // highest Byte into the first field etc
        commands[offset    ] = (byte) (converted >> 24);
        commands[offset + 1] = (byte) (converted >> 16);
        commands[offset + 2] = (byte) (converted >> 8);
        commands[offset + 3] = (byte) (converted);

        return true;
    }

    /**
     * Checks whether the Bit at position pos is set.
     * @param button Insert an int array which contains the offset at [0] and the bitposition at [1]
     * @return if Bit is set this method returns true, in any other case false.
     */
    public boolean checkBitSet(int[] button) {
        if(button.length < 2)
            return false;

        return checkBitSet(button[0], button[1]);
    }

    /**
     * Clears the Bit at the position pos
     * @param button Insert an int array which contains the offset at [0] and the bitposition at [1]
     * @return returns true if successful, else false
     */
    public boolean clearBit (int[] button){
        if(button.length < 2)
            return false;

        return clearBit(button[0], button[1]);
    }

    /**
     * getter Method for the command Array
     * @return the command-byte-array
     */
    public byte[] getCommands(){
        return commands;
    }

    /**
     * Returns the current type of the commands.
     * @return Either
     * {@link CommandHandler#TYPE_GAMEPAD}
     * {@link CommandHandler#TYPE_MOUSE},
     * {@link CommandHandler#TYPE_KEYBOARD},
     * {@link CommandHandler#TYPE_INFORMATION} or
     * {@link CommandHandler#TYPE_CONNECTION_REQUEST}
     */
    public int getMessageType(){
        return (commands[0] & 0x000F);
    }

    /**
     * Returns the current playerNo
     * @return player number as integer
     */
    public int getPlayerNumber() {
        return (commands[0] >> 4);
    }

    /**
     * If a already predefined whole package of commands is available it can be stored through this
     * method. It will override all the existing commands
     * @param commandArray Input a complete byte command array which has the expected length (as definded in Configuration_Global_Objects)
     */
    public void setCompleteCommandArray(byte[] commandArray) {
        if (commandArray.length == Configuration_Global_Objects.BYTE_ARRAY_LENGTH)
            commands = commandArray;
        else
            Log.e("ERROR", "Transferring a whole commandArray failed. Length does not fit!");

    }

    /**
     * To easily set the 2 Bytes of an Joystick Information use this method.
     * @param input The 2 Byte long byte array with already set Bit
     * @param joystick The joystick to which the commands belong
     *                 (Use {@link Configuration_Gamepad#JOYSTICK_LEFT_X },
     *                 {@link Configuration_Gamepad#JOYSTICK_LEFT_Y },
     *                 {@link Configuration_Gamepad#JOYSTICK_RIGHT_X },
     *                 {@link Configuration_Gamepad#JOYSTICK_RIGHT_Y })
     * @return Returns false if input is length unequal 2, else true after success
     */
    public boolean setJoystickData(byte[] input, int joystick){
        if (input.length != 2)
            return false;

        commands[joystick]      = input[0];
        commands[joystick +1]   = input[1];
        return true;
    }

    /**
     * Copies the first 16Byte into an Array of byte[2]. This works for all the bit Values from 0 to 2^15
     * Uses the {@link CommandHandler#saveAsByte(int, int)}
     * @param number Number which shall be moved into a
     * @return Returns an Byte array with 2 fields. The Number can be read as postion 0 is at byte[0],pos0.
     */
    public byte[] saveFirst16Bit(int number){
        byte[] b = new byte[2];
        b[0]= convertToByte(number);
        number >>= 8;
        b[1]= convertToByte(number);
        return b;
    }

    /**
     * Saves the bit values between 0 and 255 of the integer into the Byte Array into the [offset]. field
     * NOTE: Numbers above 255 will mess it up and have unexpected consequences.
     * @param offset the offset in the byte array. for entry in the first byte offset = 0, for the second offset = 1 etc
     * @param number Number which shall be converted
     * @return returns true if successful, else false
     */
    public boolean saveAsByte(int offset, int number) {
        if (offset >= BYTE_ARRAY_LENGTH)
            return false;

        commands[offset] = convertToByte(number);
        return true;
    }

    /**
     * Sets the new playerNumber
     * @param playerNumber
     */
    public void setPlayerNumber(int playerNumber){
        this.playerNumber = playerNumber;
    }

}