package com.spechtuhdelangbein.gamepad_tablet.DebugTools;

public class TimeIntervalLogger {

	private long timestamp_past = -1;
	private long timestamp_current;
    private long current_interval = 0;
    private long longest_interval = 0;
    private long average_interval = 0;
    private int max_interval_time;
	private long counter;

    /*
     * @params max_interval maximum interval length which shall be used for computing the longest interval time
     */
	public TimeIntervalLogger(int max_interval_time){
		this.max_interval_time = max_interval_time;
	}

	/**
	 * Logs the interval length between this and
	 * the previous call.
	 * If it get's called for the first time it will return 0
	 * @return the current interval length in ms
 	 */
	public long logInterval(){
		if(timestamp_past == -1){
			timestamp_current = System.nanoTime();
			return 0;
		}

		timestamp_current = System.nanoTime();
		current_interval = timestamp_current - timestamp_past;
		average_interval = (average_interval + current_interval) / 2;
		timestamp_past 	  = timestamp_current;

		if (current_interval > longest_interval && current_interval / 1000000 < max_interval_time)
            longest_interval = current_interval;

		if(counter >= Long.MAX_VALUE)
			counter = Long.MAX_VALUE - 1;

		counter ++;

		return current_interval / 1000000;

	}

	/**
	 * Logs the intervall length between this and
	 * the previous call
	 * @return the longest captured interval in ms
 	 */
	public long getLongestInterval(){
		return longest_interval;
	}


    /**
	 * Returns the average intervall time
	 * @return the average intervall length in ms
 	 */
	public long getAvgInterval(){
		return average_interval;
	}

	public long getAmountOfLogs(){
		return counter;
	}

	public void resetAmountOfLogs(){
		counter = 0;
	}
}