package com.spechtuhdelangbein.gamepad_tablet.Configurations;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.KeyEvent;
import android.view.MotionEvent;

/**
 * Created by Home on 03.09.15.
 */
public class Configuration_KeyCodes {

    private static final int xBoxDefault = -1;

    public static int KEYCODE_DPAD_UP       = KeyEvent.KEYCODE_DPAD_UP;
    public static int KEYCODE_DPAD_DOWN     = KeyEvent.KEYCODE_DPAD_DOWN;
    public static int KEYCODE_DPAD_LEFT     = KeyEvent.KEYCODE_DPAD_LEFT;
    public static int KEYCODE_DPAD_RIGHT    = KeyEvent.KEYCODE_DPAD_RIGHT;

    public static int KEYCODE_BUTTON_START  = KeyEvent.KEYCODE_BUTTON_START;
    public static int KEYCODE_BUTTON_BACK   = KeyEvent.KEYCODE_BACK;
    public static int KEYCODE_BUTTON_XBOX   = xBoxDefault;

    public static int KEYCODE_BUTTON_L1     = KeyEvent.KEYCODE_BUTTON_L1;
    public static int KEYCODE_BUTTON_R1     = KeyEvent.KEYCODE_BUTTON_R1;
    public static int KEYCODE_BUTTON_THUMBL = KeyEvent.KEYCODE_BUTTON_THUMBL;
    public static int KEYCODE_BUTTON_THUMBR = KeyEvent.KEYCODE_BUTTON_THUMBR;

    public static int KEYCODE_BUTTON_A      = KeyEvent.KEYCODE_BUTTON_A;
    public static int KEYCODE_BUTTON_B      = KeyEvent.KEYCODE_BUTTON_B;
    public static int KEYCODE_BUTTON_X      = KeyEvent.KEYCODE_BUTTON_X;
    public static int KEYCODE_BUTTON_Y      = KeyEvent.KEYCODE_BUTTON_Y;

    public static int KEYCODE_AXIS_HAT_X    = MotionEvent.AXIS_HAT_X;
    public static int KEYCODE_AXIS_HAT_Y    = MotionEvent.AXIS_HAT_Y;
    public static int KEYCODE_AXIS_X        = MotionEvent.AXIS_X;
    public static int KEYCODE_AXIS_Y        = MotionEvent.AXIS_Y;
    public static int KEYCODE_AXIS_Z        = MotionEvent.AXIS_Z;
    public static int KEYCODE_AXIS_RZ       = MotionEvent.AXIS_RZ;
    public static int KEYCODE_AXIS_LTRIGGER = MotionEvent.AXIS_LTRIGGER;
    public static int KEYCODE_AXIS_RTRIGGER = MotionEvent.AXIS_RTRIGGER;


    public static final String DPAD_UP       = "DPAD_UP";
    public static final String DPAD_DOWN     = "DPAD_DOWN";
    public static final String DPAD_LEFT     = "DPAD_LEFT";
    public static final String DPAD_RIGHT    = "DPAD_RIGHT";
    public static final String BUTTON_START  = "BUTTON_START";
    public static final String BUTTON_BACK   = "BUTTON_BACK";
    public static final String BUTTON_XBOX   = "BUTTON_XBOX";
    public static final String BUTTON_L1     = "BUTTON_L1";
    public static final String BUTTON_R1     = "BUTTON_R1";
    public static final String BUTTON_THUMBL = "BUTTON_THUMBL";
    public static final String BUTTON_THUMBR = "BUTTON_THUMBR";
    public static final String BUTTON_A      = "BUTTON_A";
    public static final String BUTTON_B      = "BUTTON_B";
    public static final String BUTTON_X      = "BUTTON_X";
    public static final String BUTTON_Y      = "BUTTON_Y";
    public static final String AXIS_HAT_X    = "DPAD_X";
    public static final String AXIS_HAT_Y    = "DPAD_Y";
    public static final String AXIS_X        = "LEFT_STICK_X";
    public static final String AXIS_Y        = "LEFT_STICK_Y";
    public static final String AXIS_Z        = "RIGHT_STICK_X";
    public static final String AXIS_RZ       = "RIGHT_STICK_Y";
    public static final String AXIS_LTRIGGER = "LEFT_TRIGGER";
    public static final String AXIS_RTRIGGER = "RIGHT_TRIGGER";


    /**
     * Initializes the field Values either with stored values or, if not previously set, with the default KeyEvent values.
     */
    public static void init(Context context){
        SharedPreferences preferences = context.getSharedPreferences(Configuration_Global_Objects.preferenceFile, Context.MODE_PRIVATE);
        KEYCODE_DPAD_UP         = preferences.getInt( DPAD_UP,         KeyEvent.KEYCODE_DPAD_UP       );
        KEYCODE_DPAD_DOWN       = preferences.getInt( DPAD_DOWN,       KeyEvent.KEYCODE_DPAD_DOWN     );
        KEYCODE_DPAD_LEFT       = preferences.getInt( DPAD_LEFT,       KeyEvent.KEYCODE_DPAD_LEFT     );
        KEYCODE_DPAD_RIGHT      = preferences.getInt( DPAD_RIGHT,      KeyEvent.KEYCODE_DPAD_RIGHT    );

        KEYCODE_BUTTON_START    = preferences.getInt( BUTTON_START,    KeyEvent.KEYCODE_BUTTON_START  );
        KEYCODE_BUTTON_BACK     = preferences.getInt( BUTTON_BACK,     KeyEvent.KEYCODE_BACK          );
        KEYCODE_BUTTON_XBOX     = preferences.getInt( BUTTON_XBOX,     -1                             );

        KEYCODE_BUTTON_L1       = preferences.getInt( BUTTON_L1,       KeyEvent.KEYCODE_BUTTON_L1     );
        KEYCODE_BUTTON_R1       = preferences.getInt( BUTTON_R1,       KeyEvent.KEYCODE_BUTTON_R1     );
        KEYCODE_BUTTON_THUMBL   = preferences.getInt( BUTTON_THUMBL,   KeyEvent.KEYCODE_BUTTON_THUMBL );
        KEYCODE_BUTTON_THUMBR   = preferences.getInt( BUTTON_THUMBR,   KeyEvent.KEYCODE_BUTTON_THUMBR );

        KEYCODE_BUTTON_A        = preferences.getInt( BUTTON_A,        KeyEvent.KEYCODE_BUTTON_A      );
        KEYCODE_BUTTON_B        = preferences.getInt( BUTTON_B,        KeyEvent.KEYCODE_BUTTON_B      );
        KEYCODE_BUTTON_X        = preferences.getInt( BUTTON_X,        KeyEvent.KEYCODE_BUTTON_X      );
        KEYCODE_BUTTON_Y        = preferences.getInt( BUTTON_Y,        KeyEvent.KEYCODE_BUTTON_Y      );

        KEYCODE_AXIS_HAT_X      = preferences.getInt( AXIS_HAT_X,      MotionEvent.AXIS_HAT_X);
        KEYCODE_AXIS_HAT_Y      = preferences.getInt( AXIS_HAT_Y,      MotionEvent.AXIS_HAT_Y);
        KEYCODE_AXIS_X          = preferences.getInt( AXIS_X,          MotionEvent.AXIS_X);
        KEYCODE_AXIS_Y          = preferences.getInt( AXIS_Y,          MotionEvent.AXIS_Y);
        KEYCODE_AXIS_Z          = preferences.getInt( AXIS_Z,          MotionEvent.AXIS_Z);
        KEYCODE_AXIS_RZ         = preferences.getInt( AXIS_RZ,         MotionEvent.AXIS_RZ);
        KEYCODE_AXIS_LTRIGGER   = preferences.getInt( AXIS_LTRIGGER,   MotionEvent.AXIS_LTRIGGER);
        KEYCODE_AXIS_RTRIGGER   = preferences.getInt( AXIS_RTRIGGER,   MotionEvent.AXIS_RTRIGGER);
    }

    /**
     * Stores the new keycode of a button to the shared preferences
     * @param button given button. Please use the Strings provided by {@link Configuration_KeyCodes}
     * @param keyCode the new keycode the button shall have
     */
    public static void storeKeyCode(Context context, String button, int keyCode){
        SharedPreferences preferences = context.getSharedPreferences(Configuration_Global_Objects.preferenceFile, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(button, keyCode);
        editor.commit();
    }

    /**
     * Resets all values back to the default
     * @param context
     */
    public static void resetCodes (Context context) {
        storeKeyCode(context, DPAD_UP,       KeyEvent.KEYCODE_DPAD_UP);
        storeKeyCode(context, DPAD_DOWN,     KeyEvent.KEYCODE_DPAD_DOWN);
        storeKeyCode(context, DPAD_LEFT,     KeyEvent.KEYCODE_DPAD_LEFT);
        storeKeyCode(context, DPAD_RIGHT,    KeyEvent.KEYCODE_DPAD_RIGHT);

        storeKeyCode(context, BUTTON_START,  KeyEvent.KEYCODE_BUTTON_START);
        storeKeyCode(context, BUTTON_BACK,   KeyEvent.KEYCODE_BACK);
        storeKeyCode(context, BUTTON_XBOX,   xBoxDefault);

        storeKeyCode(context, BUTTON_L1,     KeyEvent.KEYCODE_BUTTON_L1);
        storeKeyCode(context, BUTTON_R1,     KeyEvent.KEYCODE_BUTTON_R1);
        storeKeyCode(context, BUTTON_THUMBL, KeyEvent.KEYCODE_BUTTON_THUMBL);
        storeKeyCode(context, BUTTON_THUMBR, KeyEvent.KEYCODE_BUTTON_THUMBR);

        storeKeyCode(context, BUTTON_A,      KeyEvent.KEYCODE_BUTTON_A);
        storeKeyCode(context, BUTTON_B,      KeyEvent.KEYCODE_BUTTON_B);
        storeKeyCode(context, BUTTON_X,      KeyEvent.KEYCODE_BUTTON_X);
        storeKeyCode(context, BUTTON_Y,      KeyEvent.KEYCODE_BUTTON_Y);

        storeKeyCode(context, AXIS_HAT_X,    MotionEvent.AXIS_HAT_X);
        storeKeyCode(context, AXIS_HAT_Y,    MotionEvent.AXIS_HAT_Y);
        storeKeyCode(context, AXIS_X,        MotionEvent.AXIS_X);
        storeKeyCode(context, AXIS_Y,        MotionEvent.AXIS_Y);
        storeKeyCode(context, AXIS_Z,        MotionEvent.AXIS_Z);
        storeKeyCode(context, AXIS_RZ,       MotionEvent.AXIS_RZ);
        storeKeyCode(context, AXIS_LTRIGGER, MotionEvent.AXIS_LTRIGGER);
        storeKeyCode(context, AXIS_RTRIGGER, MotionEvent.AXIS_RTRIGGER);
    }
}
