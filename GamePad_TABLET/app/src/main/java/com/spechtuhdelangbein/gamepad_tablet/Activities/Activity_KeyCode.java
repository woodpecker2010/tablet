package com.spechtuhdelangbein.gamepad_tablet.Activities;

import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.InputDevice;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.spechtuhdelangbein.gamepad_tablet.Configurations.Configuration_KeyCodes;
import com.spechtuhdelangbein.gamepad_tablet.R;


/**
 * This class stores all the Information about the Keycodes of the Controller.
 * If you compare input to keycodes, please use the Keycodes from this class!
 *
 * By default we will use the standard keycodes for a XBox360 Gamepad.
 *
 * This object stores the currently set Keycodes in the SharedPreferences and reloads them
 * everytime you start the app.
 * This offers the functionality to change Keycodes to support different 3rd Party Gamepads, without
 * having to calibrate them every time again.
 */

public class Activity_KeyCode extends ActionBarActivity {

    private String lighter = "light";
    private String normal = "normal";
    private String light_color = "#dedede";
    private String std_color = "#b1b1b1";
    private String button = null;
    private TextView tv = null;
    private TextView val = null;

    private TextView DPAD_UP;
    private TextView DPAD_DOWN;
    private TextView DPAD_LEFT;
    private TextView DPAD_RIGHT;
    private TextView BUTTON_START;
    private TextView BUTTON_BACK;
    private TextView BUTTON_XBOX;
    private TextView BUTTON_L1;
    private TextView BUTTON_R1;
    private TextView BUTTON_THUMBL;
    private TextView BUTTON_THUMBR;
    private TextView BUTTON_A;
    private TextView BUTTON_B;
    private TextView BUTTON_X;
    private TextView BUTTON_Y;
    private TextView Reset_Button;

    private TextView DPAD_UP_VALUE;
    private TextView DPAD_DOWN_VALUE;
    private TextView DPAD_LEFT_VALUE;
    private TextView DPAD_RIGHT_VALUE;
    private TextView BUTTON_START_VALUE;
    private TextView BUTTON_BACK_VALUE;
    private TextView BUTTON_XBOX_VALUE;
    private TextView BUTTON_L1_VALUE;
    private TextView BUTTON_R1_VALUE;
    private TextView BUTTON_THUMBL_VALUE;
    private TextView BUTTON_THUMBR_VALUE;
    private TextView BUTTON_A_VALUE;
    private TextView BUTTON_B_VALUE;
    private TextView BUTTON_X_VALUE;
    private TextView BUTTON_Y_VALUE;

    private EditText keyListener;
    private LinearLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_key_code);
        layout = (LinearLayout) findViewById(R.id.keyCode_focusable_layout);
        keyListener = (EditText) findViewById(R.id.keyListener);
        keyListener.setOnKeyListener(onKeyListener);

        findTextViews();
        initValues();
        setTouchListeners();

        layout.requestFocus();
    }

    /**
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_key_code, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
*/

    private void waitForButtonPress(){
        if(button == null)
            return;
        keyListener.requestFocus();
    }

    /**
     * Changes the text color of the given text View either lighter or darker
     * @param txt Enter the {@link android.widget.TextView} which text color shall be changed
     * @param color either {@link Activity_KeyCode#lighter}
     *              or any other String like {@link Activity_KeyCode#normal}
     */
    private void changeTextColor(TextView txt, TextView val, String color){
        if(txt == null)
            return;

        if(color.equals(lighter)) {
            txt.setTextColor(Color.parseColor(light_color));
            val.setTextColor(Color.parseColor(light_color));
        }

        else {
            txt.setTextColor(Color.parseColor(std_color));
            val.setTextColor(Color.parseColor(std_color));
        }
    }

    /**
     * Calls the {@link Configuration_KeyCodes} Class to store
     * the given keyCode
     * @param button Button whose keyCode shall be changed.
     * @param keyCode new key code which shall be set
     */
    private void registerKeyCode(String button, int keyCode){
        Configuration_KeyCodes.storeKeyCode(getApplication(), button, keyCode);
    }

    /**
     * Assign TextViews to its elements
     */
    private void findTextViews(){
        DPAD_UP         = (TextView) findViewById(R.id.tv_dpadUp);
        DPAD_DOWN       = (TextView) findViewById(R.id.tv_dpadDown);
        DPAD_LEFT       = (TextView) findViewById(R.id.tv_dpadLeft);
        DPAD_RIGHT      = (TextView) findViewById(R.id.tv_dpadRight);
        BUTTON_START    = (TextView) findViewById(R.id.tv_button_start);
        BUTTON_BACK     = (TextView) findViewById(R.id.tv_button_back);
        BUTTON_XBOX     = (TextView) findViewById(R.id.tv_button_xbox);
        BUTTON_L1       = (TextView) findViewById(R.id.tv_button_l1);
        BUTTON_R1       = (TextView) findViewById(R.id.tv_button_r1);
        BUTTON_THUMBL   = (TextView) findViewById(R.id.tv_button_thumbl);
        BUTTON_THUMBR   = (TextView) findViewById(R.id.tv_button_thumbr);
        BUTTON_A        = (TextView) findViewById(R.id.tv_button_a);
        BUTTON_B        = (TextView) findViewById(R.id.tv_button_b);
        BUTTON_X        = (TextView) findViewById(R.id.tv_button_x);
        BUTTON_Y        = (TextView) findViewById(R.id.tv_button_y);
        Reset_Button    = (TextView) findViewById(R.id.tv_set_default);

        DPAD_UP_VALUE          = (TextView) findViewById(R.id.tv_dpadUp_value);
        DPAD_DOWN_VALUE        = (TextView) findViewById(R.id.tv_dpadDown_value);
        DPAD_LEFT_VALUE        = (TextView) findViewById(R.id.tv_dpadLeft_value);
        DPAD_RIGHT_VALUE       = (TextView) findViewById(R.id.tv_dpadRight_value);
        BUTTON_START_VALUE     = (TextView) findViewById(R.id.tv_button_start_value);
        BUTTON_BACK_VALUE      = (TextView) findViewById(R.id.tv_button_back_value);
        BUTTON_XBOX_VALUE      = (TextView) findViewById(R.id.tv_button_xbox_value);
        BUTTON_L1_VALUE        = (TextView) findViewById(R.id.tv_button_l1_value);
        BUTTON_R1_VALUE        = (TextView) findViewById(R.id.tv_button_r1_value);
        BUTTON_THUMBL_VALUE    = (TextView) findViewById(R.id.tv_button_thumbl_value);
        BUTTON_THUMBR_VALUE    = (TextView) findViewById(R.id.tv_button_thumbr_value);
        BUTTON_A_VALUE         = (TextView) findViewById(R.id.tv_button_a_value);
        BUTTON_B_VALUE         = (TextView) findViewById(R.id.tv_button_b_value);
        BUTTON_X_VALUE         = (TextView) findViewById(R.id.tv_button_x_value);
        BUTTON_Y_VALUE         = (TextView) findViewById(R.id.tv_button_y_value);
    }

    /**
     * Inserts the current keyCodes into the value fields
     */
    private void initValues(){
        Configuration_KeyCodes.init(getApplication());
        DPAD_UP_VALUE.setText(" = " + Configuration_KeyCodes.KEYCODE_DPAD_UP);
        DPAD_DOWN_VALUE.setText(" = " + Configuration_KeyCodes.KEYCODE_DPAD_DOWN);
        DPAD_LEFT_VALUE.setText(" = " + Configuration_KeyCodes.KEYCODE_DPAD_LEFT);
        DPAD_RIGHT_VALUE.setText(" = " + Configuration_KeyCodes.KEYCODE_DPAD_RIGHT);
        BUTTON_START_VALUE.setText(" = " + Configuration_KeyCodes.KEYCODE_BUTTON_START);
        BUTTON_BACK_VALUE.setText(" = " + Configuration_KeyCodes.KEYCODE_BUTTON_BACK);
        BUTTON_XBOX_VALUE.setText(" = " + Configuration_KeyCodes.KEYCODE_BUTTON_XBOX);
        BUTTON_L1_VALUE.setText(" = " + Configuration_KeyCodes.KEYCODE_BUTTON_L1);
        BUTTON_R1_VALUE.setText(" = " + Configuration_KeyCodes.KEYCODE_BUTTON_R1);
        BUTTON_THUMBL_VALUE.setText(" = " + Configuration_KeyCodes.KEYCODE_BUTTON_THUMBL);
        BUTTON_THUMBR_VALUE.setText(" = " + Configuration_KeyCodes.KEYCODE_BUTTON_THUMBR);
        BUTTON_A_VALUE.setText(" = " + Configuration_KeyCodes.KEYCODE_BUTTON_A);
        BUTTON_B_VALUE.setText(" = " + Configuration_KeyCodes.KEYCODE_BUTTON_B);
        BUTTON_X_VALUE.setText(" = " + Configuration_KeyCodes.KEYCODE_BUTTON_X);
        BUTTON_Y_VALUE.setText(" = " + Configuration_KeyCodes.KEYCODE_BUTTON_Y);
    }

    /**
     * Registers the onTouchListener to every TextView
     */
    private void setTouchListeners(){
        DPAD_UP.setOnTouchListener(onTouchListener);
        DPAD_DOWN.setOnTouchListener(onTouchListener);
        DPAD_LEFT.setOnTouchListener(onTouchListener);
        DPAD_RIGHT.setOnTouchListener(onTouchListener);
        BUTTON_START.setOnTouchListener(onTouchListener);
        BUTTON_BACK.setOnTouchListener(onTouchListener);
        BUTTON_XBOX.setOnTouchListener(onTouchListener);
        BUTTON_L1.setOnTouchListener(onTouchListener);
        BUTTON_R1.setOnTouchListener(onTouchListener);
        BUTTON_THUMBL.setOnTouchListener(onTouchListener);
        BUTTON_THUMBR.setOnTouchListener(onTouchListener);
        BUTTON_A.setOnTouchListener(onTouchListener);
        BUTTON_B.setOnTouchListener(onTouchListener);
        BUTTON_X.setOnTouchListener(onTouchListener);
        BUTTON_Y.setOnTouchListener(onTouchListener);
        Reset_Button.setOnTouchListener(onTouchListener);

        DPAD_UP_VALUE.setOnTouchListener(onTouchListener);
        DPAD_DOWN_VALUE.setOnTouchListener(onTouchListener);
        DPAD_LEFT_VALUE.setOnTouchListener(onTouchListener);
        DPAD_RIGHT_VALUE.setOnTouchListener(onTouchListener);
        BUTTON_START_VALUE.setOnTouchListener(onTouchListener);
        BUTTON_BACK_VALUE.setOnTouchListener(onTouchListener);
        BUTTON_XBOX_VALUE.setOnTouchListener(onTouchListener);
        BUTTON_L1_VALUE.setOnTouchListener(onTouchListener);
        BUTTON_R1_VALUE.setOnTouchListener(onTouchListener);
        BUTTON_THUMBL_VALUE.setOnTouchListener(onTouchListener);
        BUTTON_THUMBR_VALUE.setOnTouchListener(onTouchListener);
        BUTTON_A_VALUE.setOnTouchListener(onTouchListener);
        BUTTON_B_VALUE.setOnTouchListener(onTouchListener);
        BUTTON_X_VALUE.setOnTouchListener(onTouchListener);
        BUTTON_Y_VALUE.setOnTouchListener(onTouchListener);

    }

    View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {

            // avoid displayed multiselection
            if(tv != null) {
                if (val != null)
                    changeTextColor(tv, val, std_color);
                else
                    changeTextColor(tv, tv, std_color);
            }


            int source = view.getId();
            switch (source) {
                case R.id.tv_dpadUp:
                case R.id.tv_dpadUp_value:
                    button = Configuration_KeyCodes.DPAD_UP;
                    tv = DPAD_UP;
                    val = DPAD_UP_VALUE;
                    break;

                case R.id.tv_dpadDown:
                case R.id.tv_dpadDown_value:
                    button = Configuration_KeyCodes.DPAD_DOWN;
                    tv = DPAD_DOWN;
                    val = DPAD_DOWN_VALUE;
                    break;

                case R.id.tv_dpadLeft:
                case R.id.tv_dpadLeft_value:
                    button = Configuration_KeyCodes.DPAD_LEFT;
                    tv = DPAD_LEFT;
                    val = DPAD_LEFT_VALUE;
                    break;

                case R.id.tv_dpadRight:
                case R.id.tv_dpadRight_value:
                    button = Configuration_KeyCodes.DPAD_RIGHT;
                    tv = DPAD_RIGHT;
                    val = DPAD_RIGHT_VALUE;
                    break;

                case R.id.tv_button_start:
                case R.id.tv_button_start_value:
                    button = Configuration_KeyCodes.BUTTON_START;
                    tv = BUTTON_START;
                    val = BUTTON_START_VALUE;
                    break;

                case R.id.tv_button_back:
                case R.id.tv_button_back_value:
                    button = Configuration_KeyCodes.BUTTON_BACK;
                    tv = BUTTON_BACK;
                    val = BUTTON_BACK_VALUE;
                    break;

                case R.id.tv_button_xbox:
                case R.id.tv_button_xbox_value:
                    button = Configuration_KeyCodes.BUTTON_XBOX;
                    tv = BUTTON_XBOX;
                    val = BUTTON_XBOX_VALUE;
                    break;

                case R.id.tv_button_l1:
                case R.id.tv_button_l1_value:
                    button = Configuration_KeyCodes.BUTTON_L1;
                    tv = BUTTON_L1;
                    val = BUTTON_L1_VALUE;
                    break;

                case R.id.tv_button_r1:
                case R.id.tv_button_r1_value:
                    button = Configuration_KeyCodes.BUTTON_R1;
                    tv = BUTTON_R1;
                    val = BUTTON_R1_VALUE;
                    break;

                case R.id.tv_button_thumbl:
                case R.id.tv_button_thumbl_value:
                    button = Configuration_KeyCodes.BUTTON_THUMBL;
                    tv = BUTTON_THUMBL;
                    val = BUTTON_THUMBL_VALUE;
                    break;

                case R.id.tv_button_thumbr:
                case R.id.tv_button_thumbr_value:
                    button = Configuration_KeyCodes.BUTTON_THUMBR;
                    tv = BUTTON_THUMBR;
                    val = BUTTON_THUMBR_VALUE;
                    break;

                case R.id.tv_button_a:
                case R.id.tv_button_a_value:
                    button = Configuration_KeyCodes.BUTTON_A;
                    tv = BUTTON_A;
                    val = BUTTON_A_VALUE;
                    break;

                case R.id.tv_button_b:
                case R.id.tv_button_b_value:
                    button = Configuration_KeyCodes.BUTTON_B;
                    tv = BUTTON_B;
                    val = BUTTON_B_VALUE;
                    break;

                case R.id.tv_button_x:
                case R.id.tv_button_x_value:
                    button = Configuration_KeyCodes.BUTTON_X;
                    tv = BUTTON_X;
                    val = BUTTON_X_VALUE;
                    break;

                case R.id.tv_button_y:
                case R.id.tv_button_y_value:
                    button = Configuration_KeyCodes.BUTTON_Y;
                    tv = BUTTON_Y;
                    val = BUTTON_Y_VALUE;
                    break;

                case R.id.tv_set_default:
                    changeTextColor(Reset_Button, Reset_Button, lighter);
                    Configuration_KeyCodes.resetCodes(getApplicationContext());
                    initValues();
                    changeTextColor(Reset_Button, Reset_Button, normal);
                    return true;

                default:
                    button = null;
                    tv =null;
                    break;
            }

            changeTextColor(tv, val, lighter);
            waitForButtonPress();;
            return true;
        }
    };

    View.OnKeyListener onKeyListener = new View.OnKeyListener() {
        // if a keyDown comes from a gamepad register it
        // anyhow change the focus after any onKey Event
        @Override
        public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
            if(keyEvent.getAction() == KeyEvent.ACTION_DOWN && (keyEvent.getSource() & InputDevice.SOURCE_GAMEPAD) == InputDevice.SOURCE_GAMEPAD) {
                registerKeyCode(button, keyCode);
                val.setText(" = " + keyCode);
            }

            changeTextColor(tv, val, normal);

            button = null;
            tv = null;
            val = null;

            // change focus so that not every input is catched from now
            layout.requestFocus();
            return true;
        }
    };
}
