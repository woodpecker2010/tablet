package com.spechtuhdelangbein.gamepad_tablet.Configurations;

import com.spechtuhdelangbein.gamepad_tablet.Utilities.Thread_ConnectedThread;
import com.spechtuhdelangbein.gamepad_tablet.Utilities.GameView;

/**
 * This Class is used for "appwide" used Objects as an easy transfer prossibility
 * Created by Andreas on 01.07.2015.
 */
public class Configuration_Global_Objects {

    public static final int BYTE_ARRAY_LENGTH = 13;
    public static Thread_ConnectedThread mThreadConnectedThread;
    public static GameView gameView;
    public static final String languageKey = "com.spechtuhdelangbein.gamepad_tablet.language";
    public static final String preferenceFile = "com.spechtuhdelangbein.gamepad_tablet.preferences";
}
