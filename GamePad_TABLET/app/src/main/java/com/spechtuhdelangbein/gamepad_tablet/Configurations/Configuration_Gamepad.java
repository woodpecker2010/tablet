package com.spechtuhdelangbein.gamepad_tablet.Configurations;

/**
 * Configuration for basic values of the different gamepad buttons
 *
 * Created by benedikt on 21.01.17.
 */

public class Configuration_Gamepad {

    // Offset of the different Joystick & Trigger Axis'
    public final static int JOYSTICK_LEFT_X  = 5;
    public final static int JOYSTICK_LEFT_Y  = 7;
    public final static int JOYSTICK_RIGHT_X = 9;
    public final static int JOYSTICK_RIGHT_Y = 11;
    public final static int TRIGGER_LEFT     = 3;
    public final static int TRIGGER_RIGHT    = 4;

    //Positioning of the Buttons
    public final static int[] DPAD_UP = {1,0};
    public final static int[] DPAD_DOWN = {1,1};
    public final static int[] DPAD_LEFT = {1,2};
    public final static int[] DPAD_RIGHT = {1,3};
    public final static int[] START = {1,4};
    public final static int[] BACK = {1,5};
    public final static int[] LEFT_STICK_PRESS = {1,6};
    public final static int[] RIGHT_STICK_PRESS = {1,7};
    public final static int[] LB = {2,0};
    public final static int[] RB = {2,1};
    public final static int[] XBOX_LOGO = {2,2};
    public final static int[] BUTTON_A = {2,4};
    public final static int[] BUTTON_B = {2,5};
    public final static int[] BUTTON_X = {2,6};
    public final static int[] BUTTON_Y = {2,7};

    //Positioning of the Analog Sticks and Triggers
    public final static int LEFT_STICK_X  = 5;
    public final static int LEFT_STICK_Y  = 7;
    public final static int RIGHT_STICK_X = 9;
    public final static int RIGHT_STICK_Y = 11;
    public final static int LEFT_TRIGGER    = 3;
    public final static int RIGHT_TRIGGER   = 4;

    //Offset if the Axis for Touchpad Input
    public final static int XAXIS = 1;
    public final static int YAXIS = 5;
    public final static int[] leftClick = {9,0};
    public final static int[] rightCLick = {9,1};
}
