package com.spechtuhdelangbein.gamepad_tablet.Utilities;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.InputDevice;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;

import com.spechtuhdelangbein.gamepad_tablet.Activities.Activity_GameScreen;
import com.spechtuhdelangbein.gamepad_tablet.Configurations.Configuration_Gamepad;
import com.spechtuhdelangbein.gamepad_tablet.Configurations.Configuration_Global_Objects;
import com.spechtuhdelangbein.gamepad_tablet.DeviceManagers.InputManagerCompat;
import com.spechtuhdelangbein.gamepad_tablet.DeviceManagers.InputManagerCompat.InputDeviceListener;
import com.spechtuhdelangbein.gamepad_tablet.Configurations.Configuration_KeyCodes;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;


/**
 * This View shall represent the final output view.
 * Just imagine this view also displays the stream coming from the consoles.
 *
 * Here we gonna support the input of a Gamepad directly. As soon as you're integrating a View of a Game
 * (e.g. by a Surface View) please integrate it into this class or adopt the needed functions.
 *
 * It will also handle the input from a smartphone via a connected Thread.
 *
 * For debugging reasons this View will display the input commands it receives. It shows a set of
 * 0 and 1's, representing the binary structure of the protocoll. For further information about the
 * protocol itself please refer to {@link CommandHandler}
 *
 * This way we we'll be able to present this project and to prove that it's working properly
 *
 * Created by Benedikt on 08.07.2015.
 */

public class GameView extends View implements InputDeviceListener {

    /** change this value to get a bigger text. In addition you may have to change {@link GameView#SHIFT_TO_RIGHT} */
    private final float TEXT_SIZE = 10.0f;
    /** change this value to shift the display of "Type: " & "Player No: " further to the right or the left */
    private final int SHIFT_TO_RIGHT = 90;
    /** amount of supported players */
    private final int MAX_AMOUNT_PLAYERS = 4;

    public CommandHandler[] commandHandlers = new CommandHandler[MAX_AMOUNT_PLAYERS];
    /** The connection though which a communication with the smartphone is possible */
    private Thread_ConnectedThread connection;
    /** Parental Activity - Important for setting views (in-)visible */
    private Activity_GameScreen activityGameScreen;
    /** Map for up to {@link GameView#MAX_AMOUNT_PLAYERS} possible players*/
    protected Map<Integer,CommandHandler> players;
    /** Key is a natural number (1-4), its value the real number (0-3) for programming */
    private Map<Integer, Integer> playerNo;
    /** For finding out which player Numbers are already assigned */
    private boolean[] isPlayerAvailable = new boolean[MAX_AMOUNT_PLAYERS];

    private final InputManagerCompat inputManager;

    // Testing Stuff
    private String headingGamepad    = "Gamepad";
    private String headingSmartphone = "Smartphone";
    /** The deviceID */
    private int latestGamepadID      = -1;
    /** Natural Number (1-4) */
    private int latestSmartphoneID   = -1;
    /** Has to be equal to the accuracy value in the CommandHandler of the smartphone app. It allows the deal with a higher accuracy*/
    private int touchpad_value_accuracy = 100000;


    /**
     * The public constructor initializes all the private Handlers which this object will contain
     * The constructor will be automatically called on create of the host activity
     *
     * This View is currently meant to be inserted into the host activity via XML
     *
     * @param context
     * @param attributeSet
     */
    public GameView (Context context, AttributeSet attributeSet){
        super(context, attributeSet);
        setFocusable(true);
        setFocusableInTouchMode(true);

        Configuration_KeyCodes.init(getContext());

        commandHandlers = new CommandHandler[MAX_AMOUNT_PLAYERS];
        players  = new HashMap<>();
        playerNo = new HashMap<>();

        for (int i = 0; i < MAX_AMOUNT_PLAYERS; i++)
            isPlayerAvailable[i]=false;

        // Register the input Manager
        inputManager = InputManagerCompat.Factory.getInputManager(this.getContext());
        inputManager.registerInputDeviceListener(this, null);
        inputManager.getInputDeviceIds();



        // Get & link the connected thread and this instance of GameView
        // The thread should lie in the Configuration_Global_Objects class
        if(Configuration_Global_Objects.mThreadConnectedThread != null) {
            connection = Configuration_Global_Objects.mThreadConnectedThread;
            connection.setGameView(this);
        }
        else
            Configuration_Global_Objects.gameView = this;

    }


    /**
     * Handles a KeyDown Event
     * As soon as a Button is pushed on a controller it will cause an onKeyDown Event
     * This method will then set the bit belonging to the specific released button.
     *
     * It does not care whether the Bit is already set or not.
     *
     * @param keyCode the generated KeyCode
     * @param event The generating event
     * @return true if successful, else false
     */
    @Override
    public boolean onKeyDown (int keyCode, KeyEvent event){
        headingGamepad = "Down! " + event.getKeyCode();
        int deviceID = event.getDeviceId();

        // Check wether commandHandler has already been set
        // if not register the device
        if(!players.containsKey(deviceID))
                onInputDeviceAdded(deviceID);

        // if the commandHandler is called for the first time
        // initialize it first
        CommandHandler commandHandler = players.get(deviceID);
        if(commandHandler == null){
            commandHandler = new CommandHandler();
            commandHandler.setNewMessageType(CommandHandler.TYPE_GAMEPAD,playerNo.get(deviceID) + 1);
        }


        if(keyCode == Configuration_KeyCodes.KEYCODE_DPAD_UP)
            return setBitAndRefresh(commandHandler, Configuration_Gamepad.DPAD_UP);

        else if(keyCode == Configuration_KeyCodes.KEYCODE_DPAD_DOWN)
            return setBitAndRefresh(commandHandler, Configuration_Gamepad.DPAD_DOWN);

        else if(keyCode == Configuration_KeyCodes.KEYCODE_DPAD_LEFT)
            return setBitAndRefresh(commandHandler, Configuration_Gamepad.DPAD_LEFT);

        else if(keyCode == Configuration_KeyCodes.KEYCODE_DPAD_RIGHT)
            return setBitAndRefresh(commandHandler, Configuration_Gamepad.DPAD_RIGHT);

        else if(keyCode == Configuration_KeyCodes.KEYCODE_BUTTON_START)
            return setBitAndRefresh(commandHandler, Configuration_Gamepad.START);

        else if(keyCode == Configuration_KeyCodes.KEYCODE_BUTTON_BACK) {
            if (isController(event))
                return setBitAndRefresh(commandHandler, Configuration_Gamepad.BACK);
            else
                return super.onKeyUp(keyCode, event);
        }

        else if(keyCode == Configuration_KeyCodes.KEYCODE_BUTTON_L1)
            return setBitAndRefresh(commandHandler, Configuration_Gamepad.LB);

        else if(keyCode == Configuration_KeyCodes.KEYCODE_BUTTON_R1)
            return setBitAndRefresh(commandHandler, Configuration_Gamepad.RB);

        else if(keyCode == Configuration_KeyCodes.KEYCODE_BUTTON_THUMBL)
            return setBitAndRefresh(commandHandler, Configuration_Gamepad.LEFT_STICK_PRESS);

        else if(keyCode == Configuration_KeyCodes.KEYCODE_BUTTON_THUMBR)
            return setBitAndRefresh(commandHandler, Configuration_Gamepad.RIGHT_STICK_PRESS);

        else if(keyCode == Configuration_KeyCodes.KEYCODE_BUTTON_XBOX) {
            if (isController(event))
                return setBitAndRefresh(commandHandler, Configuration_Gamepad.XBOX_LOGO);
            else
                return super.onKeyUp(keyCode, event);
        }

        else if(keyCode == Configuration_KeyCodes.KEYCODE_BUTTON_A)
            return setBitAndRefresh(commandHandler, Configuration_Gamepad.BUTTON_A);

        else if(keyCode == Configuration_KeyCodes.KEYCODE_BUTTON_B)
            return setBitAndRefresh(commandHandler, Configuration_Gamepad.BUTTON_B);

        else if(keyCode == Configuration_KeyCodes.KEYCODE_BUTTON_X)
            return setBitAndRefresh(commandHandler, Configuration_Gamepad.BUTTON_X);

        else if(keyCode == Configuration_KeyCodes.KEYCODE_BUTTON_Y)
            return setBitAndRefresh(commandHandler, Configuration_Gamepad.BUTTON_Y);

        else    //Unknown Button Press
            return super.onKeyUp(keyCode, event);
    }


    /**
     * Handles a KeyUp Event.
     * As soon as a Button gets released on a controller it will cause an onKeyUp Event
     * This method will then clear the bit belonging to the specific released button
     *
     * It does not care whether the bit is already cleared or not
     *
     * @param keyCode the generated KeyCode
     * @param event The generating event
     * @return true if successful, else false
     */
    @Override
    public boolean onKeyUp (int keyCode, KeyEvent event){
        headingGamepad = "UP! " + keyCode;
        int deviceID = event.getDeviceId();

        // Check wether commandHandler has already been set
        // if not register the device
        if(!players.containsKey(deviceID))
                onInputDeviceAdded(deviceID);

        // if the commandHandler is called for the first time
        // initialize it first
        CommandHandler commandHandler = players.get(deviceID);
        if(commandHandler == null){
            commandHandler = new CommandHandler();
            commandHandler.setNewMessageType(CommandHandler.TYPE_GAMEPAD,playerNo.get(deviceID) + 1);
        }

        if(keyCode == Configuration_KeyCodes.KEYCODE_DPAD_UP)
                return clearBitAndRefresh(commandHandler, Configuration_Gamepad.DPAD_UP);

        else if(keyCode == Configuration_KeyCodes.KEYCODE_DPAD_DOWN)
                return clearBitAndRefresh(commandHandler, Configuration_Gamepad.DPAD_DOWN);

        else if(keyCode == Configuration_KeyCodes.KEYCODE_DPAD_LEFT)
                return clearBitAndRefresh(commandHandler, Configuration_Gamepad.DPAD_LEFT);

        else if(keyCode == Configuration_KeyCodes.KEYCODE_DPAD_RIGHT)
                return clearBitAndRefresh(commandHandler, Configuration_Gamepad.DPAD_RIGHT);

        else if(keyCode == Configuration_KeyCodes.KEYCODE_BUTTON_START)
                return clearBitAndRefresh(commandHandler, Configuration_Gamepad.START);

        else if(keyCode == Configuration_KeyCodes.KEYCODE_BUTTON_BACK) {
            if (isController(event))
                return clearBitAndRefresh(commandHandler, Configuration_Gamepad.BACK);
            else
                return super.onKeyDown(keyCode, event);
        }

        else if(keyCode == Configuration_KeyCodes.KEYCODE_BUTTON_L1)
                return clearBitAndRefresh(commandHandler, Configuration_Gamepad.LB);

        else if(keyCode == Configuration_KeyCodes.KEYCODE_BUTTON_R1)
                return clearBitAndRefresh(commandHandler, Configuration_Gamepad.RB);

        else if(keyCode == Configuration_KeyCodes.KEYCODE_BUTTON_THUMBL)
                return clearBitAndRefresh(commandHandler, Configuration_Gamepad.LEFT_STICK_PRESS);

        else if(keyCode == Configuration_KeyCodes.KEYCODE_BUTTON_THUMBR)
                return clearBitAndRefresh(commandHandler, Configuration_Gamepad.RIGHT_STICK_PRESS);

        else if(keyCode == Configuration_KeyCodes.KEYCODE_BUTTON_XBOX) {
            if (isController(event))
                return clearBitAndRefresh(commandHandler, Configuration_Gamepad.XBOX_LOGO);
            else
                return super.onKeyDown(keyCode, event);
        }

        else if(keyCode == Configuration_KeyCodes.KEYCODE_BUTTON_A)
                return clearBitAndRefresh(commandHandler, Configuration_Gamepad.BUTTON_A);

        else if(keyCode == Configuration_KeyCodes.KEYCODE_BUTTON_B)
                return clearBitAndRefresh(commandHandler, Configuration_Gamepad.BUTTON_B);

        else if(keyCode == Configuration_KeyCodes.KEYCODE_BUTTON_X)
                return clearBitAndRefresh(commandHandler, Configuration_Gamepad.BUTTON_X);

        else if(keyCode == Configuration_KeyCodes.KEYCODE_BUTTON_Y)
                return clearBitAndRefresh(commandHandler, Configuration_Gamepad.BUTTON_Y);

        else    //Unknown Button Press
                return super.onKeyUp(keyCode, event);
    }


    /**
     * Handles motion events.
     * Motion Events can be caused by Joysticks als well as by a DPad
     * It will check every value from the possible sources and set them into the command Array
     *
     * @param event method calling Event
     * @return returns true if successful command extraction
     */
    @Override
    public boolean onGenericMotionEvent (MotionEvent event) {
        inputManager.onGenericMotionEvent(event);

        headingGamepad = "Motion! X-Axis:" + event.getAxisValue(MotionEvent.AXIS_X);
        int deviceID = event.getDeviceId();

        // if a device is deconnected while still sending a motionEvent (e.g. in cause of the sticks)
        // the app would crash. So before handling the motion event, check whether device is still available
        if (event.getDevice() != null) {

            // Check wether commandHandler has already been set
            // If this is correct, register a new commandHandler for the device
            if (!players.containsKey(deviceID))
                onInputDeviceAdded(deviceID);

            // if the commandHandler is called for the first time
            // initialize it first
            CommandHandler commandHandler = players.get(deviceID);
            if (commandHandler == null) {
                commandHandler = new CommandHandler();
                commandHandler.setNewMessageType(CommandHandler.TYPE_GAMEPAD, playerNo.get(deviceID) + 1);
            }


            // DPad Movements
            float dPad_xAxis = getCenteredAxis(event, event.getDevice(), MotionEvent.AXIS_HAT_X);
            float dPad_yAxis = getCenteredAxis(event, event.getDevice(), MotionEvent.AXIS_HAT_Y);

            // Left stick movements
            float left_xAxis = getCenteredAxis(event, event.getDevice(), MotionEvent.AXIS_X);
            float left_yAxis = getCenteredAxis(event, event.getDevice(), MotionEvent.AXIS_Y);

            // Right Stick movements
            float right_xAxis = getCenteredAxis(event, event.getDevice(), MotionEvent.AXIS_Z);
            float right_yAxis = getCenteredAxis(event, event.getDevice(), MotionEvent.AXIS_RZ);

            // Left Trigger & Right Trigger movements
            float left_trigger = getCenteredAxis(event, event.getDevice(), MotionEvent.AXIS_LTRIGGER);
            float right_Trigger = getCenteredAxis(event, event.getDevice(), MotionEvent.AXIS_RTRIGGER);


            // Computing Values for the Joysticks
            // we have 32767 Numbers we can represent
            // => Step intervall: ~0.000030518509475
            // Divide the float number through intervallstep and
            // convert the output to an 2 Byte long ByteArray
            // To be handled as signed!
            byte[] leftX = commandHandler.saveFirst16Bit((int) (left_xAxis / 0.000030518509475f));
            byte[] leftY = commandHandler.saveFirst16Bit((int) (left_yAxis / 0.000030518509475f));
            byte[] rightX = commandHandler.saveFirst16Bit((int) (right_xAxis / 0.000030518509475f));
            byte[] rightY = commandHandler.saveFirst16Bit((int) (right_yAxis / 0.000030518509475f));


            // computing values for the triggers
            // we have 255 Values we can represent
            // => Step intervall: ~0.00392156862
            // to be handled as unsigned!
            int leftTrigger = (int) (left_trigger / 0.00392156862f);
            int rightTrigger = (int) (right_Trigger / 0.00392156862f);


            // Store the data
            commandHandler.setJoystickData(leftX, Configuration_Gamepad.JOYSTICK_LEFT_X);
            commandHandler.setJoystickData(leftY, Configuration_Gamepad.JOYSTICK_LEFT_Y);
            commandHandler.setJoystickData(rightX, Configuration_Gamepad.JOYSTICK_RIGHT_X);
            commandHandler.setJoystickData(rightY, Configuration_Gamepad.JOYSTICK_RIGHT_Y);
            commandHandler.saveAsByte(Configuration_Gamepad.TRIGGER_LEFT, leftTrigger);
            commandHandler.saveAsByte(Configuration_Gamepad.TRIGGER_RIGHT, rightTrigger);

            if (dPad_xAxis == 0) {
                commandHandler.clearBit(Configuration_Gamepad.DPAD_LEFT);
                commandHandler.clearBit(Configuration_Gamepad.DPAD_RIGHT);
            } else if (dPad_xAxis == 1) {
                commandHandler.setBit(Configuration_Gamepad.DPAD_RIGHT);
                commandHandler.clearBit(Configuration_Gamepad.DPAD_LEFT);
            } else if (dPad_xAxis == -1) {
                commandHandler.setBit(Configuration_Gamepad.DPAD_LEFT);
                commandHandler.clearBit(Configuration_Gamepad.DPAD_RIGHT);
            }

            if (dPad_yAxis == 0) {
                commandHandler.clearBit(Configuration_Gamepad.DPAD_DOWN);
                commandHandler.clearBit(Configuration_Gamepad.DPAD_UP);
            } else if (dPad_yAxis == 1) {
                commandHandler.setBit(Configuration_Gamepad.DPAD_DOWN);
                commandHandler.clearBit(Configuration_Gamepad.DPAD_UP);
            } else if (dPad_yAxis == -1) {
                commandHandler.setBit(Configuration_Gamepad.DPAD_UP);
                commandHandler.clearBit(Configuration_Gamepad.DPAD_DOWN);
            }

            invalidate();
            return true;
        }
        return false;
    }


    /**
     * Check whether current event was created through a Gamepad with Buttons & Joystick
     *
     * @param event Event which shall be tested
     * @return returns if Event is made by a gamepad
     */
    private boolean isController (KeyEvent event) {
        if ((event.getSource() & InputDevice.SOURCE_GAMEPAD) == InputDevice.SOURCE_GAMEPAD)
            return true;
        return false;
    }


    /**
     * Checks whether current calling device has a joystick
     *
     * @param event event, who's device shall be checked
     * @return True if it has a joystick, else false
     */
    @Deprecated
    private boolean isJoystickDevice (MotionEvent event){
        return ((event.getSource() & InputDevice.SOURCE_JOYSTICK)
                == InputDevice.SOURCE_JOYSTICK
                &&  event.getAction() == MotionEvent.ACTION_MOVE);
    }


    /**
     * This method is to make sure that our joystick movement isn't in the flat range
     * which shall not be treated as an input. (as all joysticks may send corrupt data while
     * resting in the default position)
     *
     * @param event The calling Event
     * @param device The device of the calling event (normally event.getDevice()
     * @param axis the axis which shall be tested (like: MotionEvent.AXIS_X)
     * @return returns 0 if it's in flat range, or the actual value
     */
    private static float getCenteredAxis(MotionEvent event, InputDevice device, int axis) {
        // sometimes this method is exactly then called as the device got plugged out
        if(device == null)
            return 0f;
        final InputDevice.MotionRange range = device.getMotionRange(axis, event.getSource());

        //check whether we are in the flat range of the controller
        if(range != null) {
            final float flat = range.getFlat();
            final float value = event.getAxisValue(axis);

            if(Math.abs(value) > flat)
                return value;
        }
        return 0;
    }


    /**
     * Sets the requested Bit and calls with its invalidate() statement the onDraw method
     * So that the displayed information is refreshed
     *
     * @param commandHandler needs the gamepadHandler which is to modified
     * @param button the button which called this event
     * @return always returns true
     */
    private boolean setBitAndRefresh(CommandHandler commandHandler, int[] button){
        commandHandler.setBit(button);
        invalidate();
        requestLayout();
        return true;
    }


    /**
     * Clears the requested Bit and calls with its invalidate() statement the onDraw method
     * So that the displayed information is refreshed
     *
     * @param commandHandler needs the gamepadHandler which is to modified
     * @param button the button which called this event
     * @return always returns true
     */
    private boolean clearBitAndRefresh(CommandHandler commandHandler, int[] button){
        commandHandler.clearBit(button);
        invalidate();
        requestLayout();
        return true;
    }

    /**
     * The onDraw method refreshes the currently displayed View.
     *
     * The current content is just for debugging and presentation usage.
     *
     * It displays 2 structures which appear to be 2 fields.
     * One represents the input from the smartphone, the other one the input of the Gamepad.
     * Make sure that as soon as inputs change you call the invalidate() method to force a call of
     * this method.
     */
    @Override
    protected void onDraw(Canvas canvas){
        super.onDraw(canvas);
        Paint paint = new Paint();

        // Setting up a textsize
        // Scale it so that it supports multiple screens with different ppi
        float dip = TEXT_SIZE;
        float scale = getContext().getResources().getDisplayMetrics().density;
        float textSize = dip * scale + 0.5f;
        paint.setTextSize(textSize);
        paint.setColor(Color.WHITE);

        int screenMiddle = getWidth() / 2;
        int posX = 0;
        int posY = 0;

        drawGamepadPackageData(canvas,paint,dip,scale,screenMiddle,posX,posY);
        drawSmartphonePackageData(canvas,paint,dip,scale,screenMiddle,posX,posY);
    }

    /** Setter Method - to set the internal Activity_GameScreen */
    public void setActivityGameScreen(Activity_GameScreen activity){
        activityGameScreen = activity;
    }

    /**
     * If the given deviceID isn't registered, this method will register it
     * and assign an empty Instance of {@link GameView#commandHandlers} to it.
     * @param deviceId insert the unique id of the device
     */
    @Override
    public void onInputDeviceAdded(int deviceId) {
        // If this device is unknown, add it.
        if(!players.containsKey(deviceId)) {
            for(int i = 0; i < MAX_AMOUNT_PLAYERS; i++){
                if(!isPlayerAvailable[i]) {
                    isPlayerAvailable[i] = true;
                    commandHandlers[i] = new CommandHandler();
                    commandHandlers[i].setNewMessageType(CommandHandler.TYPE_GAMEPAD, i + 1);
                    players.put(deviceId, commandHandlers[i]);
                    playerNo.put(deviceId,i);
                    // Debug
                    commandHandlers[i].setNewMessageType(CommandHandler.TYPE_GAMEPAD, i +1);
                    int newPlayerNumber = i+1;
                    Log.i("GameView", "Added wired Player " + newPlayerNumber);
                    latestGamepadID = deviceId;
                    break;
                }
            }
        }
    }

    @Override
    public void onInputDeviceChanged(int deviceId) {

    }

    @Override
    public void onInputDeviceRemoved(int deviceId) {
        if(players != null){
            if(players.containsKey(deviceId)) {
                int removedRealPlayerNo = playerNo.get(deviceId).intValue();
                isPlayerAvailable[removedRealPlayerNo] = false;
                commandHandlers[removedRealPlayerNo] = new CommandHandler();
                players.remove(deviceId);
                playerNo.remove(deviceId);
                Log.i("GameView", "Disconnected wired Player " + (removedRealPlayerNo + 1));
            }
        }
        invalidate();
    }

    /**
     * Sets up a player number for a newly connected smartphone.
     */
    public void onSmartphoneAdded (){

        if (Configuration_Global_Objects.mThreadConnectedThread != null)
            connection = Configuration_Global_Objects.mThreadConnectedThread;
        connection.setGameView(this);

        for (int i = 0; i < MAX_AMOUNT_PLAYERS; i++) {
            if (!isPlayerAvailable[i]) {
                isPlayerAvailable[i] = true;
                int newPlayerNumber  = i+1;
                playerNo.put(newPlayerNumber, i);

                // send the player no to the Smartphone
                // it'll be registered with its natural number (1-4)
                // while the value will be the real number (0 - 3)
                byte[] information = new byte[Configuration_Global_Objects.BYTE_ARRAY_LENGTH];
                information[0] = (byte) (information[0] | CommandHandler.TYPE_INFORMATION << 0);
                information[0] = (byte) (information[0] | newPlayerNumber << 4);
                connection.write(information);

                // register new player
                Log.i("GameView", "Added new Player: " + newPlayerNumber);
                // Debug things
                commandHandlers[i] = new CommandHandler();
                connection.setCommandHandler(commandHandlers[i]);
                //commandHandlers[i] = connection.getCommandHandler();
                latestSmartphoneID = newPlayerNumber;
                break;
            }
        }
    }

    /**
     * Removes information about the currently removed smartphone
     */
    public void onSmartphoneRemoved(int playerNo){
        if (playerNo >= 1 && playerNo <= 4) {
            isPlayerAvailable[playerNo - 1] = false;
            this.playerNo.remove(playerNo);
        } else
            Log.e("CONNCETION", "Failed to remove Smartphone from listed players");

        invalidate();
    }

    /**
     * if a smartphone quits without sending it's quit code
     */
    public void onForcedSmartphoneRemoved(){
        Log.i("Game View", "Smartphone quit without notification");
        isPlayerAvailable[latestSmartphoneID -1] = false;
        playerNo.remove(latestSmartphoneID);
        // Debug reasons
        commandHandlers[latestSmartphoneID - 1] = new CommandHandler();
    }

    /**
     * DEBUG METHOD
     * Just for dummy representations
     * @param canvas current canvas
     * @param paint current paint data
     * @param dip DIP of the font
     * @param scale factor to scale
     */
    private void drawRealData(Canvas canvas, Paint paint, float dip, float scale, int screenMiddle, String handlerName) {
        // Convert Raw Data into Real Data
        // SMARTPHONE DATA
        int posX;
        CommandHandler handler;
        if (handlerName == "Smartphone" || handlerName == "smartphone") {
            posX = SHIFT_TO_RIGHT;
            int i = playerNo.get(latestSmartphoneID).intValue();
            handler = commandHandlers[i];
        } else {
            posX = screenMiddle + SHIFT_TO_RIGHT;
            int i = playerNo.get(latestGamepadID);
            handler = commandHandlers[i];
        }

        // Print out MessageType
        String txt = "Type not set";
        int help = (int) handler.getCommands()[0] & 0x0F;
        if (help == handler.TYPE_GAMEPAD)
            txt = "Gamepad";
        else if (help == handler.TYPE_KEYBOARD)
            txt = "Keyboard";
        else if (help == handler.TYPE_MOUSE)
            txt = "Mouse";
        canvas.drawText("Type: "+txt, posX + 300, 180 +0 *(int) dip *scale, paint);

        // Print out PlayerNumber
        help = (int) handler.getCommands()[0]>>4;
        canvas.drawText("Player No " + help, posX + 300, 180 + 1 * (int) dip * scale, paint);

        //Print out Button Data
        if(handler.checkBitSet(Configuration_Gamepad.DPAD_UP))
            canvas.drawText("DPAD UP",      posX,180 +0 *(int) dip *scale, paint);
        if(handler.checkBitSet(Configuration_Gamepad.DPAD_DOWN))
            canvas.drawText("DPAD DOWN",    posX,180 +1 *(int) dip *scale, paint);
        if(handler.checkBitSet(Configuration_Gamepad.DPAD_LEFT))
            canvas.drawText("DPAD LEFT",    posX,180 +2 *(int) dip *scale, paint);
        if(handler.checkBitSet(Configuration_Gamepad.DPAD_RIGHT))
            canvas.drawText("DPAD RIGHT",   posX,180 +3 *(int) dip *scale, paint);
        if(handler.checkBitSet(Configuration_Gamepad.START))
            canvas.drawText("START",        posX,180 +4 *(int) dip *scale, paint);
        if(handler.checkBitSet(Configuration_Gamepad.BACK))
            canvas.drawText("BACK",         posX,180 +5 *(int) dip *scale, paint);
        if(handler.checkBitSet(Configuration_Gamepad.LEFT_STICK_PRESS))
            canvas.drawText("LEFT STICK",   posX,180 +6 *(int) dip *scale, paint);
        if(handler.checkBitSet(Configuration_Gamepad.RIGHT_STICK_PRESS))
            canvas.drawText("RIGHT STICK",  posX,180 +7 *(int) dip *scale, paint);
        if(handler.checkBitSet(Configuration_Gamepad.LB))
            canvas.drawText("LB",           posX,180 +8 *(int) dip *scale, paint);
        if(handler.checkBitSet(Configuration_Gamepad.RB))
            canvas.drawText("RB",           posX,180 +9 *(int) dip *scale, paint);
        if(handler.checkBitSet(Configuration_Gamepad.XBOX_LOGO))
            canvas.drawText("XBOX LOGO",    posX,180 +10 *(int) dip *scale, paint);
        if(handler.checkBitSet(Configuration_Gamepad.BUTTON_A))
            canvas.drawText("BUTTON A",     posX,180 +11 *(int) dip *scale, paint);
        if(handler.checkBitSet(Configuration_Gamepad.BUTTON_B))
            canvas.drawText("BUTTON B",     posX,180 +12 *(int) dip *scale, paint);
        if(handler.checkBitSet(Configuration_Gamepad.BUTTON_X))
            canvas.drawText("BUTTON X",     posX,180 +13 *(int) dip *scale, paint);
        if(handler.checkBitSet(Configuration_Gamepad.BUTTON_Y))
            canvas.drawText("BUTTON Y",     posX,180 +14 *(int) dip *scale, paint);

        // Trigger Data
        byte[] bytes = handler.getCommands();
        int value = (int) bytes[3];
        canvas.drawText("LEFT TRIGGER: \t" + value, posX, 180 +15 *(int) dip *scale, paint);
        value = (int) bytes[4];
        canvas.drawText("RIGHT TRIGGER:" + value, posX, 180 +16 *(int) dip *scale, paint);

        // Joystick Data
        int result = (bytes[5]<<0 | 0xFF00);
        int helper = (bytes[6]<<8 | 0x00FF);
        if(bytes[6]<0)
            result = (result | 0xFFFF0000);
        result = result & helper;
        float data = result * 0.000030518509475f;
        canvas.drawText("LEFT STICK - X:\t\t"+ data, posX, 180 +17 *(int) dip *scale, paint);

        result = (bytes[7]<<0 | 0xFF00);
        helper = (bytes[8]<<8 | 0x00FF);
        if(bytes[8]<0)
            result = (result | 0xFFFF0000);
        result = result & helper;
        data = result * 0.000030518509475f;
        canvas.drawText("LEFT STICK - Y:\t\t"+ data, posX, 180 +18 *(int) dip *scale, paint);

        result = (bytes[9]<<0 | 0xFF00);
        helper = (bytes[10]<<8 | 0x00FF);
        if(bytes[10]<0)
            result = (result | 0xFFFF0000);
        result = result & helper;
        data = result * 0.000030518509475f;
        canvas.drawText("RIGHT STICK - X: "+ data, posX, 180 +19 *(int) dip *scale, paint);

        result = (bytes[11]<<0 | 0xFF00);
        helper = (bytes[12]<<8 | 0x00FF);
        if(bytes[12]<0)
            result = (result | 0xFFFF0000);
        result = result & helper;
        data = result * 0.000030518509475f;
        canvas.drawText("RIGHT STICK - Y: "+ data, posX, 180 +20 *(int) dip *scale, paint);
    }

    /**
     * DEBUG METHOD.
     * Shows the actual Gamepad Data Package on screen
     */
    private void drawGamepadPackageData(Canvas canvas, Paint paint, float dip, float scale, int screenMiddle, int posX, int posY){
        String bytesGamepad[] = new String[Configuration_Global_Objects.BYTE_ARRAY_LENGTH];

        // Draw all the Gamepad Data
        // if no Gamepad Data is available draw N/A instead
        if (playerNo != null && playerNo.containsKey(latestGamepadID)) {
            int pos = playerNo.get(latestGamepadID);
            for (int i = 0; i < Configuration_Global_Objects.BYTE_ARRAY_LENGTH; i++) {
                posY = (int) (180 + i * (int) dip * scale);
                bytesGamepad[i] = String.format("%8s", Integer.toBinaryString(commandHandlers[pos].getCommands()[i] & 0xFF)).replace(' ', '0');
                canvas.drawText(bytesGamepad[i], screenMiddle, posY, paint);
            }
            drawRealData(canvas, paint, dip, scale, screenMiddle, "Gamepad");
        } else {
            // Case that no GamepadData is available
            headingGamepad = "Gamepad";
            canvas.drawText("N/A", screenMiddle, 180, paint);
        }

        // Draw the heading - shows weither "Gamepad" or the actual received input
        canvas.drawText(headingGamepad, screenMiddle, 80, paint);
    }

    /**
     * DEBUG METHOD.
     * Shows the actual Smartphone Data Package on screen
     */
    private void drawSmartphonePackageData(Canvas canvas, Paint paint, float dip, float scale, int screenMiddle, int posX, int posY){
        String bytesSmartphone[] = new String[Configuration_Global_Objects.BYTE_ARRAY_LENGTH];

        //
        // Draw all Smartphone Date
        // if data is corrupt, don't show
        // if no data available show N/A
        if(playerNo != null && playerNo.containsKey(latestSmartphoneID)) {

            int pos = playerNo.get(latestSmartphoneID).intValue();
            CommandHandler commandHandler = commandHandlers[pos];
            if (commandHandler == null)
                Log.i("GameView","Smartphone commandhandler is a Nullpointer");

            // don't display broken command packages
            if ((commandHandler.getCommands()[0] & 0x0F) == 0 || ((commandHandler.getCommands()[0] & 0xFF) == 0xFF)) {
                Log.e("Parsing", "Received Corrupt Package");
                return;
            }

            // Collect the command information of every single byte and display it
            // The layout shall be:
            // Show the smartphone commands on the left hand side and
            // Show the gamepad commands on the right hand side with the middle as it's left border
            for (int i = 0; i < Configuration_Global_Objects.BYTE_ARRAY_LENGTH; i++) {
                posY = (int) (180 + i * (int) dip * scale);

                bytesSmartphone[i] = String.format("%8s", Integer.toBinaryString(commandHandler.getCommands()[i] & 0xFF)).replace(' ', '0');
                canvas.drawText(bytesSmartphone[i], posX, posY, paint);

            }
            drawRealData(canvas, paint, dip, scale, screenMiddle, "Smartphone");


            // DEBUG ISSUE
            // if we have a mouse command display the mouse movements
            // Note that the movement values have to bee divided by 100.000 to receive
            // the real movement. (This was introduced due to an accuracy issue)
            int messageType = commandHandler.getCommands()[0] & 0x0F;
            if (messageType == CommandHandler.TYPE_MOUSE) {
                byte[] commands = commandHandler.getCommands();
                byte[] xAxis = new byte[4];
                byte[] yAxis = new byte[4];

                // compute X and Y Axis Values of the touchpad income
                xAxis[0] = commands[Configuration_Gamepad.XAXIS];
                xAxis[1] = commands[Configuration_Gamepad.XAXIS + 1];
                xAxis[2] = commands[Configuration_Gamepad.XAXIS + 2];
                xAxis[3] = commands[Configuration_Gamepad.XAXIS + 3];
                int addX     = ByteBuffer.wrap(xAxis).getInt();
                float f_addX = (float) addX / touchpad_value_accuracy;
                yAxis[0] = commands[Configuration_Gamepad.YAXIS];
                yAxis[1] = commands[Configuration_Gamepad.YAXIS + 1];
                yAxis[2] = commands[Configuration_Gamepad.YAXIS + 2];
                yAxis[3] = commands[Configuration_Gamepad.YAXIS + 3];
                int addY     = ByteBuffer.wrap(yAxis).getInt();
                float f_addY = (float) addY / touchpad_value_accuracy;

                if (f_addX != 0 || f_addY != 0)
                    Log.i("TouchIncome: ", "X: " + f_addX + "| Y: " + f_addY);

                if (activityGameScreen != null)
                    activityGameScreen.refreshTouchpadView(f_addX, f_addY);

                // if we have a keyboard command display the current typed char
            } else if (messageType == CommandHandler.TYPE_KEYBOARD) {
                // Convert Input to a String
                // Actually a single character will lie in this String
                byte[] commands = commandHandler.getCommands();
                byte[] character = new byte[2];
                String excerptedChar = "undefined";
                character[0] = commands[1];
                character[1] = commands[2];

                // refresh the displayed string only
                // if a new character was set
                if (character[0] != 0 || character[1] != 0)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
                        excerptedChar = new String(character, StandardCharsets.ISO_8859_1);
                    else
                        excerptedChar = new String(character);

                if (activityGameScreen != null)
                    activityGameScreen.refreshKeyboardView(excerptedChar);
            } else {
                if (activityGameScreen != null)
                    activityGameScreen.setTouchpadViewInvisilbe();
                activityGameScreen.setCharacterViewInvisible();
            }
        } else {
            // Case that no Smartphone Data is availabe
            canvas.drawText("N/A", posX, 180, paint);
        }

        // Show the Heading "Smartphone"
        canvas.drawText(headingSmartphone, 0, 80, paint);

    }
}
