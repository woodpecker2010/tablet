package com.spechtuhdelangbein.gamepad_tablet.Utilities;

import android.bluetooth.BluetoothSocket;
import android.util.Log;

import com.spechtuhdelangbein.gamepad_tablet.Activities.Activity_Main;
import com.spechtuhdelangbein.gamepad_tablet.Configurations.Configuration_Global_Objects;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Andreas on 06.06.2015.
 * Thread_ConnectedThread is called from the Thread_AcceptingThread class.
 * Since Reading from the incoming Data is an action that blocks the entire application
 * I relayed it onto its own thread.
 * TODO: getGamepad commands
 * TODO: Method DongleSink (change attributes?)
 * TODO: Implement DongleSink in final full application
 * TODO: Remove for the final full application:
 */
public class Thread_ConnectedThread extends Thread {

    // set this value to the lengh of the expected incoming Byte Array
    private final int BYTE_ARRAY_LENGTH = Configuration_Global_Objects.BYTE_ARRAY_LENGTH;
    private final BluetoothSocket m_BTSocket;
    private final InputStream m_InputStream;
    private final OutputStream m_OutputStream;

    Activity_Main m_mainact;
    boolean connected = false;

    private boolean running = true;

    /** Stores the incoming data */
    public byte[] buffer = new byte[BYTE_ARRAY_LENGTH];
    /** CommandHandler to whom all the command information is transferred */
    private CommandHandler commandHandler;
    /** GameView which displays the information ~ REMOVE IN FINAL COMPLETE APP*/
    private GameView gameView;



    public BluetoothSocket GetSocket()
    {
        return m_BTSocket;
    }

    public Thread_ConnectedThread(BluetoothSocket socket, Activity_Main ma) {
        Log.d("Milestone","In connected thread");
        m_mainact = ma;
        m_BTSocket = socket;
        commandHandler = new CommandHandler();
        InputStream tmpIn = null;
        OutputStream tmpOut = null;

        // Get the input and output streams, using temp objects because
        // member streams are final
        try {
            tmpIn = socket.getInputStream();
            tmpOut = socket.getOutputStream();
        } catch (IOException e) { }

        m_InputStream = tmpIn;
        m_OutputStream = tmpOut;

        // Set this Thread available for other Objects through
        // registering it staticly in the Configuration_Global_Objects class
        connected = true;
        Configuration_Global_Objects.mThreadConnectedThread = this;
        m_mainact.Connected();

        if (Configuration_Global_Objects.gameView != null)
            gameView = Configuration_Global_Objects.gameView;
    }

    public void run() {

        // amount of bytes returned from read()
        int bytes;


        if(running) {

            /*
                // for caputring the intervall time between the arrival of data packages
                TimeIntervalLogger logger = new TimeIntervalLogger(80);
            */

            // Keep listening to the InputStream until an exception occurs
            // TODO: store Data for DongleSink-Method!
            while (true) {
                /*
                    logger.logInterval();
                    if (logger.getAmountOfLogs() == 1000) {
                        logger.resetAmountOfLogs();
                        Log.i("Connected Thread Report", "Avg Receiving Intervall: " + logger.getAvgInterval());
                        Log.i("Connected Thread Report", "Max Receiving Intervall: " + logger.getLongestInterval());
                    }
                */

                try {
                    // Read from the InputStream BYTE_ARRAY_LENGTH Bytes
                    bytes = m_InputStream.read(buffer, 0, BYTE_ARRAY_LENGTH);
                    // TODO: The buffer now contains the current protocol file. Transfer it to wherever you need it!

                    // transfer this information to the commandHandler if possible
                    // and refresh the view
                    if (commandHandler != null) {
                        commandHandler.setCompleteCommandArray(buffer);

                        if (gameView != null) {
                            // Check on special MessageType for registration
                            if (commandHandler.getMessageType() == CommandHandler.TYPE_CONNECTION_REQUEST)
                                gameView.onSmartphoneAdded();
                            if (commandHandler.getMessageType() == CommandHandler.TYPE_QUIT_CONNECTION)
                                gameView.onSmartphoneRemoved(commandHandler.getPlayerNumber());

                            try {
                                gameView.postInvalidate();
                            } catch (NullPointerException e) {
                                Log.i("Thread_ConnectedThread", "Invalidating gameView threw a Nullpointer Exception");
                                // Send a KillCode to the smartphone to interrupt it's connection process
                                byte[] killSmartphone = new byte[BYTE_ARRAY_LENGTH];
                                killSmartphone[0] = CommandHandler.TYPE_QUIT_CONNECTION;
                                try {
                                    m_OutputStream.write(killSmartphone);
                                } catch (Exception ex) {
                                    Log.e("Thread_ConnectedThread", "Unable to send a QUIT_CONNECTION command to smartphone");
                                }
                            }
                        }
                    } else
                        Log.i("Connected Thread: ", "No commandHandler found.");
                } catch (IOException ioe) {
                    connected = false;
                    if(gameView != null)
                        gameView.onForcedSmartphoneRemoved();
                    Log.d("Connected Thread", "Cannot read.Lost connection");
                    cancel();
                    break;
                }

            }
        }
        else
        {
            this.interrupt();
        }
    }

    /**
     * REMOVE ~ DEBUG & PRESENTATION USAGE!
     * For setting the commandHandler which will be used to store the information
     * @param handler
     */
    public void setCommandHandler (CommandHandler handler){
        commandHandler = handler;
    }

    public CommandHandler getCommandHandler() {
        return commandHandler;
    }


    /**
     * Sets the GameView
     * @param view insert a gameview
     */
    public void setGameView (GameView view) {
        gameView = view;
        Configuration_Global_Objects.gameView = view;
    }

    /** Call this from the main activity to send data to the remote device */
    public boolean write(byte[] bytes) {
        try {
            m_OutputStream.write(bytes);
            return true;
        } catch (IOException e)
        {
            Log.d("Milestone","Cannot write. Lost connection");
            connected = false;
            cancel();
            return false;
        }
    }

    /** Call this from the main activity to shutdown the connection */
    public void cancel() {
        try {
            m_BTSocket.close();
            m_mainact.Disconnected();
            running = false;
            Log.d("Milestone", "Closed connection");
           // m_mainact.startConnectionActivity(null);
            this.interrupt();
        } catch (IOException e) { }
    }

    /**
     * The dongle sink is the final halt for input data
     * from our end.
     * This is where Gazoo should take all incoming data packages
     * and send them to the dongle
     * It contains an ArrayList with the last received datapackages.
     * **/
    public void DongleSink(ArrayList<String> RResults) {
        /*
            TODO
            At this point we can use the ArrayList "ReceivedResults" and take the
            oldest entry to send it to the XBOX-Dongle.
            It is important to remove entries from the front as newer Inputs are added
            at the end of the ArrayList.
            If the ArrayList grows to large we might have a bad connection and can remove some
            of the older entries. This deletes older pressed input commands to react to the
            newer ones
        */

        Calendar c = Calendar.getInstance();
        int currHours = c.get(Calendar.HOUR);
        int currMins = c.get(Calendar.MINUTE);
        int currSeconds = c.get(Calendar.SECOND);

        String currTime = String.valueOf(currHours)+":"+String.valueOf(currMins)+":"+String.valueOf(currSeconds);

        /*for(String tmp : RResults) {
            m_mainact.ChangeDebugTxt(tmp+" \n"+currTime);
        }*/

        RResults.clear();
    }
}
