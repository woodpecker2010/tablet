package com.spechtuhdelangbein.gamepad_tablet.DebugTools;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.spechtuhdelangbein.gamepad_tablet.R;
import com.spechtuhdelangbein.gamepad_tablet.Utilities.GameView;

/**
 * This view simply displays the currently sent character.
 * It won't only display what the current package is sending, but remain on the character until
 * a new one is sent.
 *
 * Displaying a char correctly will only work on Android API 19 ( KITKAT) or higher!
 * As displaying this on the screen is not essential for our App to work there's no workaround
 * for lower versions
 * Take a look at the method {@link GameView#onDraw(Canvas)} in
 * {@link GameView}
 *
 * Created by Benedikt Specht on 08.08.15.
 */
public class CharacterView extends View {
    private int height = 1200;
    private int width = 1200;
    private int posX = 300;
    private int posY = 300;
    private int textSize = 200;
    private String currentCharacter = "undefined";
    private Paint paint;
    private Bitmap background;


    public CharacterView(Context context, AttributeSet attrs) {
        super(context, attrs);
        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.WHITE);
        paint.setTextSize(textSize);
        background = BitmapFactory.decodeResource(getResources(), R.drawable.half_transparent);
        invalidate();
    }

    public void changeChar(String character){
        currentCharacter = character;
        invalidate();
    }

    public void onDraw(Canvas canvas){
        super.onDraw(canvas);
        //draw a semi transparent background
        canvas.drawBitmap(background, 0, 0, paint);
        canvas.drawText(currentCharacter,posX,posY,paint);
    }
}
