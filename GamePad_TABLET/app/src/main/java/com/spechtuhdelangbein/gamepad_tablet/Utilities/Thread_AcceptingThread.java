package com.spechtuhdelangbein.gamepad_tablet.Utilities;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import com.spechtuhdelangbein.gamepad_tablet.Activities.Activity_Main;

import java.io.IOException;
import java.util.UUID;

/**
 * Created by Andreas on 06.06.2015.
 * The Thread_AcceptingThread handles incoming connections and creates a bluetooth server that
 * a client can connect to.
 */
public class Thread_AcceptingThread extends Thread {

    public boolean running = true;
    private final BluetoothServerSocket myLittleServerSocket;

    Activity_Main m_mainact;

    public Thread_AcceptingThread(BluetoothAdapter mBluetoothAdapter, Activity_Main ma) {

        Log.d("Milestone","In accepting thread..");
        m_mainact = ma;
        BluetoothServerSocket tmp = null;
        try {
            tmp = mBluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord("GamePad", UUID.fromString("38400000-8cf0-11bd-b23e-10b96e4ef00d"));
        } catch (IOException e) { }
        myLittleServerSocket = tmp;
    }

    public void run() {

        if(running) {
            BluetoothSocket socket = null;
            // Keep listening until exception occurs or a socket is returned
            while (true) {
                try {
                    socket = myLittleServerSocket.accept();
                } catch (Exception e) {
                    Log.d("Milestone", "Exception");
                    break;
                }
                // If a connection was accepted
                if (socket != null) {
                    // Do work to manage the connection (in a separate thread)
                    manageConnectedSocket(socket);
                    try {
                        myLittleServerSocket.close();
                    } catch (IOException iox) {
                        Log.e("IOException", "The Serversocket can't be closed");
                    }
                    break;
                }
            }
        }
        else
        {
            this.interrupt();
        }
    }

    /** Will cancel the listening socket, and cause the thread to finish */
    public void cancel() {
        try {
            myLittleServerSocket.close();
        } catch (IOException e) { }
    }

    public void manageConnectedSocket(BluetoothSocket sock)
    {
        Log.d("Good","The server has a connected bluetoothsocket");
        Log.d("Good","Starting connected thread");
        Thread_ConnectedThread ct = new Thread_ConnectedThread(sock,m_mainact);
        m_mainact.m_conth = ct;
        ct.start();
        this.interrupt();
        running = false;
    }
}

